package main

func Greeting() string {
	return "Hello"
}

func Goodbye(name string) string {
	text := "Goodbye, " + name + "..."
	return text
}

func Add(a int, b int) int {
	return a + b
}

func main() {
}
