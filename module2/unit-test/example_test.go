package main

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGreeting(t *testing.T) {
	expectedResult := "Hello"
	receivedResult := Greeting()
	assert.Equal(t, expectedResult, receivedResult)
}

func TestGoodBye(t *testing.T) {
	expectedResult := "Goodbye, John..."
	receivedResult := Goodbye("John")
	assert.Equal(t, expectedResult, receivedResult)
}

func TestAdd(t *testing.T) {
	type testCase struct {
		a        int
		b        int
		expected int
	}
	testCases := []testCase{
		{a: 2, b: 2, expected: 4},
		{a: 1, b: 4, expected: 5},
	}

	for _, test := range testCases {
		expectedResult := test.expected
		receivedResult := Add(test.a, test.b)
		assert.Equal(t, expectedResult, receivedResult)

		fmt.Printf("case : a = %d, b = %d while expected result equals to %d, "+"recaived result equals to %d\n",
			test.a, test.b, test.expected, receivedResult)
	}
}

func TestAddWithTableDrivenTesting(t *testing.T) {

	type args struct {
		a int
		b int
	}
	type testCase struct {
		name string
		args args
		want int
	}

	tests := []testCase{
		{
			name: "should adding correctly a and b when they are equal",
			args: args{a: 2, b: 2},
			want: 4,
		},
		{
			name: "should adding correctly a and b when a is less than b",
			args: args{a: 1, b: 3},
			want: 4,
		},
		{
			name: "should adding correctly a and b when a is bigger than b",
			args: args{a: 5, b: 3},
			want: 8,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			assert.Equal(t, test.want, Add(test.args.a, test.args.b), "Add(%v, %v)", test.args.a, test.args.b)
		})
	}
}
