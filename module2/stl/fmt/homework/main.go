package main

import (
	"fmt"
)

type Person struct {
	Name  string
	Age   int
	Money float64
}

func main() {
	p := Person{Name: "Andy", Age: 18, Money: 203.455}

	// вывод значений структуры
	fmt.Println("simple struct:", p)

	// вывод названий полей и их значений
	fmt.Printf("detailed struct: %+v\n", p)

	// вывод названий полей и их значений в виде инициализации
	fmt.Printf("Golang struct: %#v\n", p)

	fmt.Println(generateSelfStory(p.Name, p.Age, p.Money))
}

func generateSelfStory(name string, age int, money float64) string {
	return fmt.Sprintf("Привет! Моё имя %s, и мне %d лет. Так вышло, что я сегодня нашёл на улице кошелёк, там было аж %f рублей. Я их честно отдал в полицию. Вот такой %s ответственный гражданин!\n", name, age, money, name)
}
