package main

import (
	"fmt"
	"strconv"
	"strings"
)

type Cache struct {
	data map[string]*User
}

func NewCache() *Cache {
	return &Cache{data: make(map[string]*User, 100)}
}

func (c *Cache) Set(key string, u *User) {
	c.data[key] = u
}

func (c *Cache) Get(key string) *User {
	return c.data[key]
}

type User struct {
	ID       int
	Nickname string
	Email    string
}

func main() {
	var users []*User
	emails := []string{"robpike@gmail.com", "davecheney@gmail.com", "bradfitzpatrick@email.ru", "eliben@gmail.com", "quasilyte@mail.ru"}
	for i := range emails {
		users = append(users, &User{
			ID: i + 1,
			//Здесь правильно ввёл функцию, но получил ошибку:
			//cannot use strings.Split(emails[i], "@") (value of type []string) as string value in struct literal
			//Ошибка исправилась когда добавил [0]. То есть взял уже не слайс строк emails, а 0 элемент слайса(до "@")
			//Не видел похожих примеров, пока не понимаю как это работает:)
			Nickname: strings.Split(emails[i], "@")[0],
			Email:    emails[i],
		})
	}
	cache := NewCache()
	for _, user := range users {
		// Положить пользователей в кэш с ключом Nickname:userid
		cache.Set(strings.Join([]string{user.Nickname, strconv.Itoa(user.ID)}, ":"), user)
	}
	keys := []string{"robpike:1", "davecheney:2", "bradfitzpatrick:3", "eliben:4", "quasilyte:5"}
	for i := range keys {
		fmt.Println(cache.Get(keys[i]))
	}
}
