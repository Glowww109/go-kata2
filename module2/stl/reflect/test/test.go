package main

import (
	"fmt"
	"reflect"
)

func isEmpty(val interface{}) bool {
	return reflect.ValueOf(val).IsZero()
}

func setValue(structure interface{}, key string, value interface{}) {
	if reflect.TypeOf(structure).Elem().Kind() == reflect.Struct {
		elem := reflect.ValueOf(structure).Elem()
		field := elem.FieldByName(key)
		if field.CanSet() {
			field.Set(reflect.ValueOf(value))
		}
	}
}

func getValue(structure interface{}, key string) interface{} {
	var result interface{}
	if reflect.TypeOf(structure).Elem().Kind() == reflect.Struct {
		elem := reflect.ValueOf(structure).Elem()
		field := elem.FieldByName(key)
		if field.IsValid() {
			result = field.Interface()
		}
	}

	return result
}

func concat(a interface{}, b interface{}) interface{} {
	if reflect.TypeOf(a).Kind() == reflect.Int && reflect.TypeOf(b).Kind() == reflect.Int {
		return reflect.ValueOf(a).Int() + reflect.ValueOf(b).Int()
	}

	if reflect.TypeOf(a).Kind() == reflect.String && reflect.TypeOf(b).Kind() == reflect.String {
		return reflect.ValueOf(a).String() + reflect.ValueOf(b).String()
	}

	return nil
}

type People struct {
	Name string
	Age  int
}

func main() {
	a := 2
	b := 3
	c := "hello"
	d := "mike"
	f := concat(a, b)
	g := concat(c, d)

	fmt.Println(f)
	fmt.Println(g)

	sam := People{Name: "Sam", Age: 22}
	refSam := reflect.ValueOf(sam)
	valid := refSam.Field(1).CanSet()
	fmt.Println(refSam, valid)

	emp := isEmpty(sam)
	setValue(&sam, "Name", "Misha")
	val := getValue(&sam, "Name")
	fmt.Println(emp, sam, val)

	//Вот так дико меняется значение
	var x float64 = 3.4
	p := reflect.ValueOf(&x)
	v := p.Elem()
	v.SetFloat(7)

	fmt.Println(v.CanSet())
	fmt.Println(p.Elem())

	//Для карты

	map1 := make(map[string]int)
	map1["1"] = 1
	map1["2"] = 2
	i := 9
	tm := reflect.MapOf(reflect.TypeOf("Hello"), reflect.TypeOf(&i).Elem())
	fmt.Println(tm)

	map1Ref := reflect.ValueOf(&map1)
	m1P := map1Ref.Elem()

	for i, key := range m1P.MapKeys() {
		fmt.Println(i, key)
	}
}
