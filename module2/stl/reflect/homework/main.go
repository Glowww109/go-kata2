package main

import (
	"fmt"
	"reflect"
	"time"
)

var reflectedStructs map[string][]Field

type Field struct {
	Name string
	Tags map[string]string
}

type UserDTO struct {
	ID            int       `json:"id" db:"id" db_type:"BIGSERIAL primary key" db_default:"not null"`
	Name          string    `json:"name" db:"name" db_type:"varchar(55)" db_default:"default null" db_ops:"create,update"`
	Phone         string    `json:"phone" db:"phone" db_type:"varchar(34)" db_default:"default null" db_index:"index,unique" db_ops:"create,update"`
	Email         string    `json:"email" db:"email" db_type:"varchar(89)" db_default:"default null" db_index:"index,unique" db_ops:"create,update"`
	Password      string    `json:"password" db:"password" db_type:"varchar(144)" db_default:"default null" db_ops:"create,update"`
	Status        int       `json:"status" db:"status" db_type:"int" db_default:"default 0" db_ops:"create,update"`
	Role          int       `json:"role" db:"role" db_type:"int" db_default:"not null" db_ops:"create,update"`
	Verified      bool      `json:"verified" db:"verified" db_type:"boolean" db_default:"not null" db_ops:"create,update"`
	EmailVerified bool      `json:"email_verified" db:"email_verified" db_type:"boolean" db_default:"not null" db_ops:"create,update"`
	PhoneVerified bool      `json:"phone_verified" db:"phone_verified" db_type:"boolean" db_default:"not null" db_ops:"create,update"`
	CreatedAt     time.Time `json:"created_at" db:"created_at" db_type:"timestamp" db_default:"default (now()) not null" db_index:"index"`
	UpdatedAt     time.Time `json:"updated_at" db:"updated_at" db_type:"timestamp" db_default:"default (now()) not null" db_index:"index"`
	DeletedAt     time.Time `json:"deleted_at" db:"deleted_at" db_type:"timestamp" db_default:"default null" db_index:"index"`
}

type EmailVerifyDTO struct {
	ID        int       `json:"id" db:"id" db_type:"BIGSERIAL primary key" db_default:"not null"`
	Email     string    `json:"email" db:"email" db_type:"varchar(89)" db_default:"not null" db_index:"index,unique" db_ops:"create,update"`
	UserID    int       `json:"user_id,omitempty" db:"user_id" db_ops:"create" db_type:"int" db_default:"not null" db_index:"index"`
	Hash      string    `json:"hash,omitempty" db:"hash" db_ops:"create" db_type:"char(36)" db_default:"not null" db_index:"index"`
	Verified  bool      `json:"verified" db:"verified" db_type:"boolean" db_default:"not null" db_ops:"create,update"`
	CreatedAt time.Time `json:"created_at" db:"created_at" db_type:"timestamp" db_default:"default (now()) not null" db_index:"index"`
}

func main() {
	structs := []interface{}{
		UserDTO{},
		EmailVerifyDTO{},
	}

	// заполни данными структур согласно заданиям

	reflectedStructs = make(map[string][]Field, len(structs))
	var val []Field

	//1. Произведи рефлексию структуры, создай карту (map) структур из предоставленного слайса по ссылке.
	//Ключом карты будет строковый тип со значением названия структуры, значением карты будет слайс полей структуры Field ([]Field).
	//на 84 строке выполнение пункта 1.

	for _, structV := range structs {
		key := reflect.TypeOf(structV).Name()

		//2. Заполни поля структуры Field, где поле Name — название поля,
		//поле Tags — карта тэгов с типом map[string]string, где ключом будет название тэга,
		//значением должно быть значение данного тэга.

		mapT := make(map[string]string)

		//Здесь отдельно создаём список с ключами.
		//Пробовал делать перебор через strings или regexp, получается очень массивно
		tList := []string{"json", "db", "db_type", "db_default", "db_ops", "db_index"}

		fieldNumb := reflect.TypeOf(structV).NumField()
		for i := 0; i < fieldNumb; i++ {
			name := reflect.TypeOf(structV).Field(i).Name

			tags := reflect.TypeOf(structV).Field(i).Tag
			for i := 0; i < len(tList); i++ {
				tagValue, ok := tags.Lookup(tList[i])
				if ok {
					mapT[tList[i]] = tagValue
				}
			}
			val = append(val, Field{
				Name: name,
				Tags: mapT,
			})
			reflectedStructs[key] = val
		}
		fmt.Println(reflectedStructs)
	}
}
