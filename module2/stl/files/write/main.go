package main

import (
	"bufio"
	"fmt"
	"os"

	"github.com/essentialkaos/translit/v2"
)

func main() {
	//1.Напиши программу, записывающую данные приходящие с os.Stdin
	scannedText, err := os.Create("name.txt")
	check(err)
	defer scannedText.Close()
	fmt.Print("Enter your name: ")
	reader := bufio.NewReader(os.Stdin)
	name, _ := reader.ReadString('\n')
	fmt.Printf("Hello %s\n", name)
	_, err = scannedText.WriteString(name)
	check(err)

	//2.Напиши программу, переводящую все русские символы в транслит,
	//входящий файл example.txt и example.processed.txt на выходе.
	//Текст для файла выбери произвольно.

	text := "Широкая электрификация южных губерний даст мощный толчок подъёму сельского хозяйства"

	//запись в файл
	textToFile(text)

	//перевод символов в транслит и запись в файл example.processed.txt
	//функция принимает на вход имя файла, в котором содержится текст для транслита
	textToTranslate("example.txt")
}

func check(err error) {
	if err != nil {
		os.Exit(1)
	}
}

func textToFile(s string) {
	file, err := os.Create("example.txt")
	check(err)
	defer file.Close()

	_, err = file.WriteString(s)
	check(err)
}

func textToTranslate(filename string) {
	file, err := os.Create("example.processed.txt")
	check(err)
	defer file.Close()

	content, err := os.ReadFile(filename)
	check(err)

	contentTrans := translit.EncodeToBGN(string(content))
	_, err = file.WriteString(contentTrans)
	check(err)
}
