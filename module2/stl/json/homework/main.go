package main

import (
	"encoding/json"
	"fmt"
)

type Profile struct {
	Username  string `json:"uname"`
	Followers int    `json:"followers,omitempty,string"`
}

type Student struct {
	FirstName string  `json:"fname"`           //fname - its field name
	LastName  string  `json:"lname,omitempty"` //discard if vaule nil
	Email     string  `json:"-"`               //always discard
	Age       int     `json:"-,"`              //always discard  "-" - as field name
	IsMale    bool    `json:",string"`
	Profile   Profile `json:""` //no effect
}

func main() {
	john := &Student{
		FirstName: "John",
		LastName:  "",
		Email:     "johndoe@mail.com",
		Age:       21,
		Profile: Profile{
			Username:  "johndoe91",
			Followers: 1975,
		},
	}
	johnJSON, _ := json.MarshalIndent(john, "", "")

	fmt.Println(string(johnJSON))
}
