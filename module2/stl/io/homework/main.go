package main

import (
	"bytes"
	"fmt"
	"os"
)

func main() {
	data := []string{
		"there is 3pm, but im still alive to write this code snippet",
		"чистый код лучше, чем заумный код",
		"ваш код станет наследием будущих программистов",
		"задумайтесь об этом",
	}
	// здесь расположите буфер
	var writer bytes.Buffer

	// запишите данные в буфер
	for _, p := range data {
		_, err := writer.Write([]byte(p))
		checkErr(err)
	}
	// создайте файл
	file, err := os.Create("example.txt")
	checkErr(err)
	defer file.Close()

	// запишите данные в файл
	for _, line := range data {
		_, err = file.WriteString(line + "\n")
		checkErr(err)
	}

	// прочтите данные в новый буфер.
	//Не уверен, что тут правильно понял задание.
	buffer := bytes.NewBufferString("")
	for _, line := range data {
		_, err = buffer.WriteString(line + "\n")
		checkErr(err)
	}

	//Оставлю тут, не знаю, для чего этот вывод :)
	//fmt.Println(data)

	// выведите данные из буфера buffer.String()
	fmt.Println(buffer.String())

	// 3. В конце программы выведи содержимое файла через fmt.Println.
	text, err := os.ReadFile("example.txt")
	checkErr(err)
	fmt.Println(string(text))
}

func checkErr(err error) {
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
}
