package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"
)

type Config struct {
	AppName    string `json:"appName"`
	Production bool   `json:"production"`
}

func main() {
	conf := Config{}
	//Сам флаг для ввода конфигурационного файла
	fileFlag := flag.String("conf", "default", "Ввод названия нужного файла")
	flag.Parse()

	//если введено верно - всё успешно проходит
	if *fileFlag != "default" {
		file, err := os.Open(*fileFlag)
		if err != nil {
			fmt.Println("Конфигурационный файл не принят")
			os.Exit(1)
		}
		defer file.Close()
		decoder := json.NewDecoder(file)
		err = decoder.Decode(&conf)
		checkErr(err)
		//выставляем true, для продакшена
		conf.Production = true
		fmt.Printf("Production: %v\n", conf.Production)
		fmt.Printf("%v: моё тестовое приложение\n", conf.AppName)
	} else {
		fmt.Println("Конфигурационный файл не принят")
		os.Exit(1)
	}
}

func checkErr(err error) {
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
}

// Неверный вариант, оставляю для себя.
//
//	conf := Config{}
//	//создаём переменную fullPath в которой будет записан наш введённый флаг
//	var fullPath string
//	//в переменную args записываем аргументы через os.Args то есть то, что вводится в коммандную строку как флаг
//	args := os.Args
//
//	//запускаем цикл, в процессе записываем все аргументы в переменную fullpath.
//	for i := 0; i < len(args)-1; i++ {
//		fullPath += args[i+1]
//	}
//	//Если записанный флаг равен -./configapp-conf./config.json, то мы:
//	//1. Открываем файл config.json в папке "configapp -conf ."
//	//2. Создаём декодер для открытого файла config.json
//	//3. Декодируем информацию из файла в в структуру conf(Congig{})
//	//4. Меняем conf.Production на true
//	//5. И выводим текст в консоль согласно условию
//	if fullPath == "-./configapp-conf./config.json" {
//		newJson, err := os.Open("./configapp -conf ./config.json")
//		checkErr(err)
//		defer newJson.Close()
//		decoder := json.NewDecoder(newJson)
//		err = decoder.Decode(&conf)
//		checkErr(err)
//
//		conf.Production = true
//		fmt.Printf("Production: %v\n", conf.Production)
//		fmt.Printf("%v: моё тестовое приложение\n", conf.AppName)
//
//	} else {
//		fmt.Println("Конфигурационный файл не принят")
//	}
//}
