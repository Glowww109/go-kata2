package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMultiply1(t *testing.T) {
	testCalc := calc{
		a:      1,
		b:      2,
		result: 2,
	}
	expectedResult := float64(2)
	receivedResult := multiply(testCalc.a, testCalc.b)
	assert.Equal(t, expectedResult, receivedResult)
}

func TestMultiply2(t *testing.T) {
	testCalc := calc{
		a:      2,
		b:      6,
		result: 12,
	}
	expectedResult := float64(12)
	receivedResult := multiply(testCalc.a, testCalc.b)
	assert.Equal(t, expectedResult, receivedResult)
}

func TestMultiply3(t *testing.T) {
	testCalc := calc{
		a:      (2 * 45),
		b:      (64 / 4),
		result: 12,
	}
	expectedResult := float64(1440)
	receivedResult := multiply(testCalc.a, testCalc.b)
	assert.Equal(t, expectedResult, receivedResult)
}

func TestMultiply4(t *testing.T) {
	testCalc := calc{
		a:      344444,
		b:      99995,
		result: 12,
	}
	expectedResult := float64(34442677780)
	receivedResult := multiply(testCalc.a, testCalc.b)
	assert.Equal(t, expectedResult, receivedResult)
}

func TestMultiply5(t *testing.T) {
	testCalc := calc{}
	for i := 0; i <= 10; i++ {
		sample := float64(i)
		testCalc.a = sample + 2
		testCalc.b = sample * 6
		expectedResult := (sample + 2) * (sample * 6)
		receivedResult := multiply(testCalc.a, testCalc.b)
		assert.Equal(t, expectedResult, receivedResult)
	}

}

func TestMultiply6(t *testing.T) {
	testCalc := calc{}
	arrayOfThen := [10]byte{2, 4, 6, 9, 12, 20, 56, 60, 39, 105}
	for item := range arrayOfThen {
		testCalc.a = float64(item)
		testCalc.b = testCalc.a + 3
		expectedResult := testCalc.a * testCalc.b
		receivedResult := multiply(testCalc.a, testCalc.b)
		assert.Equal(t, expectedResult, receivedResult)
	}
	_ = arrayOfThen
}

func TestMultiply7(t *testing.T) {
	testCalc := calc{}
	testMap := make(map[int]float64, 10)
	for key, value := range testMap {
		testMap[key+1] = value + 1
		testCalc.a = testMap[key+1]
		testCalc.b = testMap[key+2]
		expectedResult := testCalc.a * testCalc.b
		receivedResult := multiply(testCalc.a, testCalc.b)
		assert.Equal(t, expectedResult, receivedResult)
	}
}

func TestMultiply8(t *testing.T) {
	testCalc := calc{}
	testSlice := make([]uint8, 50)
	testSlice2 := make([]uint8, 100)
	for i, elem := range testSlice {
		testSlice2 = append(testSlice, uint8(i))
		_ = elem
	}
	testCalc.a = float64(testSlice2[40])
	testCalc.b = float64(testSlice2[23])
	expectedResult := testCalc.a * testCalc.b
	receivedResult := multiply(testCalc.a, testCalc.b)
	assert.Equal(t, expectedResult, receivedResult)
}

func TestDivide1(t *testing.T) {
	testCalc := calc{
		a:      1,
		b:      2,
		result: 2,
	}
	expectedResult := float64(2)
	receivedResult := multiply(testCalc.a, testCalc.b)
	assert.Equal(t, expectedResult, receivedResult)
}

func TestDivide2t(t *testing.T) {
	testCalc := calc{
		a:      2,
		b:      6,
		result: 12,
	}
	expectedResult := float64(12)
	receivedResult := multiply(testCalc.a, testCalc.b)
	assert.Equal(t, expectedResult, receivedResult)
}

func TestDivide3(t *testing.T) {
	testCalc := calc{
		a:      (2 * 45),
		b:      (64 / 4),
		result: 12,
	}
	expectedResult := float64(1440)
	receivedResult := multiply(testCalc.a, testCalc.b)
	assert.Equal(t, expectedResult, receivedResult)
}

func TestDivide4(t *testing.T) {
	testCalc := calc{
		a:      344444,
		b:      99995,
		result: 12,
	}
	expectedResult := float64(34442677780)
	receivedResult := multiply(testCalc.a, testCalc.b)
	assert.Equal(t, expectedResult, receivedResult)
}

func TestDivide5(t *testing.T) {
	testCalc := calc{}
	for i := 0; i <= 10; i++ {
		sample := float64(i)
		testCalc.a = sample + 2
		testCalc.b = sample * 6
		expectedResult := (sample + 2) * (sample * 6)
		receivedResult := multiply(testCalc.a, testCalc.b)
		assert.Equal(t, expectedResult, receivedResult)
	}

}

func TestDivide6(t *testing.T) {
	testCalc := calc{}
	arrayOfThen := [10]byte{2, 4, 6, 9, 12, 20, 56, 60, 39, 105}
	for item := range arrayOfThen {
		testCalc.a = float64(item)
		testCalc.b = testCalc.a + 3
		expectedResult := testCalc.a * testCalc.b
		receivedResult := multiply(testCalc.a, testCalc.b)
		assert.Equal(t, expectedResult, receivedResult)
	}
	_ = arrayOfThen
}

func TestDivide7(t *testing.T) {
	testCalc := calc{}
	testMap := make(map[int]float64, 10)
	for key, value := range testMap {
		testMap[key+1] = value + 1
		testCalc.a = testMap[key+1]
		testCalc.b = testMap[key+2]
		expectedResult := testCalc.a * testCalc.b
		receivedResult := multiply(testCalc.a, testCalc.b)
		assert.Equal(t, expectedResult, receivedResult)
	}
}

func TestDivide8(t *testing.T) {
	testCalc := calc{}
	testSlice := make([]uint8, 50)
	testSlice2 := make([]uint8, 100)
	for i, elem := range testSlice {
		testSlice2 = append(testSlice, uint8(i))
		_ = elem
	}
	testCalc.a = float64(testSlice2[40])
	testCalc.b = float64(testSlice2[23])
	expectedResult := testCalc.a * testCalc.b
	receivedResult := multiply(testCalc.a, testCalc.b)
	assert.Equal(t, expectedResult, receivedResult)
}

func TestSum1(t *testing.T) {
	testCalc := calc{
		a:      1,
		b:      2,
		result: 2,
	}
	expectedResult := float64(2)
	receivedResult := multiply(testCalc.a, testCalc.b)
	assert.Equal(t, expectedResult, receivedResult)
}

func TestSum2(t *testing.T) {
	testCalc := calc{
		a:      2,
		b:      6,
		result: 12,
	}
	expectedResult := float64(12)
	receivedResult := multiply(testCalc.a, testCalc.b)
	assert.Equal(t, expectedResult, receivedResult)
}

func TestSum3(t *testing.T) {
	testCalc := calc{
		a:      (2 * 45),
		b:      (64 / 4),
		result: 12,
	}
	expectedResult := float64(1440)
	receivedResult := multiply(testCalc.a, testCalc.b)
	assert.Equal(t, expectedResult, receivedResult)
}

func TestSum4(t *testing.T) {
	testCalc := calc{
		a:      344444,
		b:      99995,
		result: 12,
	}
	expectedResult := float64(34442677780)
	receivedResult := multiply(testCalc.a, testCalc.b)
	assert.Equal(t, expectedResult, receivedResult)
}

func TestSum5(t *testing.T) {
	testCalc := calc{}
	for i := 0; i <= 10; i++ {
		sample := float64(i)
		testCalc.a = sample + 2
		testCalc.b = sample * 6
		expectedResult := (sample + 2) * (sample * 6)
		receivedResult := multiply(testCalc.a, testCalc.b)
		assert.Equal(t, expectedResult, receivedResult)
	}

}

func TestSum6(t *testing.T) {
	testCalc := calc{}
	arrayOfThen := [10]byte{2, 4, 6, 9, 12, 20, 56, 60, 39, 105}
	for item := range arrayOfThen {
		testCalc.a = float64(item)
		testCalc.b = testCalc.a + 3
		expectedResult := testCalc.a * testCalc.b
		receivedResult := multiply(testCalc.a, testCalc.b)
		assert.Equal(t, expectedResult, receivedResult)
	}
	_ = arrayOfThen
}

func TestSum7(t *testing.T) {
	testCalc := calc{}
	testMap := make(map[int]float64, 10)
	for key, value := range testMap {
		testMap[key+1] = value + 1
		testCalc.a = testMap[key+1]
		testCalc.b = testMap[key+2]
		expectedResult := testCalc.a * testCalc.b
		receivedResult := multiply(testCalc.a, testCalc.b)
		assert.Equal(t, expectedResult, receivedResult)
	}
}

func TestSum8(t *testing.T) {
	testCalc := calc{}
	testSlice := make([]uint8, 50)
	testSlice2 := make([]uint8, 100)
	for i, elem := range testSlice {
		testSlice2 = append(testSlice, uint8(i))
		_ = elem
	}
	testCalc.a = float64(testSlice2[40])
	testCalc.b = float64(testSlice2[23])
	expectedResult := testCalc.a * testCalc.b
	receivedResult := multiply(testCalc.a, testCalc.b)
	assert.Equal(t, expectedResult, receivedResult)
}

func TestAverage1(t *testing.T) {
	testCalc := calc{
		a:      1,
		b:      2,
		result: 2,
	}
	expectedResult := float64(2)
	receivedResult := multiply(testCalc.a, testCalc.b)
	assert.Equal(t, expectedResult, receivedResult)
}

func TestAverage2(t *testing.T) {
	testCalc := calc{
		a:      2,
		b:      6,
		result: 12,
	}
	expectedResult := float64(12)
	receivedResult := multiply(testCalc.a, testCalc.b)
	assert.Equal(t, expectedResult, receivedResult)
}

func TestAverage3(t *testing.T) {
	testCalc := calc{
		a:      (2 * 45),
		b:      (64 / 4),
		result: 12,
	}
	expectedResult := float64(1440)
	receivedResult := multiply(testCalc.a, testCalc.b)
	assert.Equal(t, expectedResult, receivedResult)
}

func TestAverage4(t *testing.T) {
	testCalc := calc{
		a:      344444,
		b:      99995,
		result: 12,
	}
	expectedResult := float64(34442677780)
	receivedResult := multiply(testCalc.a, testCalc.b)
	assert.Equal(t, expectedResult, receivedResult)
}

func TestAverage5(t *testing.T) {
	testCalc := calc{}
	for i := 0; i <= 10; i++ {
		sample := float64(i)
		testCalc.a = sample + 2
		testCalc.b = sample * 6
		expectedResult := (sample + 2) * (sample * 6)
		receivedResult := multiply(testCalc.a, testCalc.b)
		assert.Equal(t, expectedResult, receivedResult)
	}

}

func TestAverage6(t *testing.T) {
	testCalc := calc{}
	arrayOfThen := [10]byte{2, 4, 6, 9, 12, 20, 56, 60, 39, 105}
	for item := range arrayOfThen {
		testCalc.a = float64(item)
		testCalc.b = testCalc.a + 3
		expectedResult := testCalc.a * testCalc.b
		receivedResult := multiply(testCalc.a, testCalc.b)
		assert.Equal(t, expectedResult, receivedResult)
	}
	_ = arrayOfThen
}

func TestAverage7(t *testing.T) {
	testCalc := calc{}
	testMap := make(map[int]float64, 10)
	for key, value := range testMap {
		testMap[key+1] = value + 1
		testCalc.a = testMap[key+1]
		testCalc.b = testMap[key+2]
		expectedResult := testCalc.a * testCalc.b
		receivedResult := multiply(testCalc.a, testCalc.b)
		assert.Equal(t, expectedResult, receivedResult)
	}
}

func TestAverage8(t *testing.T) {
	testCalc := calc{}
	testSlice := make([]uint8, 50)
	testSlice2 := make([]uint8, 100)
	for i, elem := range testSlice {
		testSlice2 = append(testSlice, uint8(i))
		_ = elem
	}
	testCalc.a = float64(testSlice2[40])
	testCalc.b = float64(testSlice2[23])
	expectedResult := testCalc.a * testCalc.b
	receivedResult := multiply(testCalc.a, testCalc.b)
	assert.Equal(t, expectedResult, receivedResult)
}
