package main

type calc struct {
	a, b   float64
	result float64
}

func NewCalc() *calc { // конструктор калькулятора
	return &calc{}
}

func (c *calc) SetA(a float64) calc {
	(*c).a = a
	return *c
}

func (c *calc) SetB(b float64) calc {
	(*c).b = b
	return *c
}

func (c *calc) Do(operation func(a, b float64) float64) calc {
	(*c).result = operation(c.a, c.b)
	return *c
}

func (c *calc) Result() float64 {
	b := c.result
	return b
}

func multiply(a, b float64) float64 {
	return a * b
}

//func divide(a, b float64) float64 {
//	return a / b
//}

//func sum(a, b float64) float64 {
//	return a + b
//}

//func average(a, b float64) float64 {
//	return (a + b) / 2
//}

func main() {
	/*	calc := NewCalc()
		res := calc.SetA(10)
		res = calc.SetB(34)
		res = calc.Do(multiply)
		fmt.Println(res)
		res2 := calc.Result()
		fmt.Println(res2)
		if res.result != res2 {
			panic("object statement is not persist")
		}
		//деление
		res = calc.Do(divide)
		fmt.Println(res)
		res2 = calc.Result()
		fmt.Println(res2)
		if res.result != res2 {
			panic("object statement is not persist")
		}
		//сложение
		res = calc.Do(sum)
		fmt.Println(res)
		res2 = calc.Result()
		fmt.Println(res2)
		if res.result != res2 {
			panic("object statement is not persist")
		}
		//среднее значение
		res = calc.Do(average)
		fmt.Println(res)
		res2 = calc.Result()
		fmt.Println(res2)
		if res.result != res2 {
			panic("object statement is not persist")
		}
	*/
}
