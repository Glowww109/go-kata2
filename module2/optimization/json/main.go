package main

//По итогу 4х бенчмарков, библиотека jsoniter:
//1.В сериализации медленнее стандартной почти на 53849%,
//хотя аллокация памяти практически такая же.
//2. В десиариализации ситуация наоборот, стандартная библиотека медленнее на 150%.
//По итогу данного теста можно сделать вывод, что для десиариализации однозначно
//лучше использовать jsoniter, а для сериализации лучше оставить стандартную библиотеку.

import (
	"encoding/json"

	jsoniter "github.com/json-iterator/go"
)

type PetsPark []PetsParkElement

func UnmarshalPetsPark(data []byte) (PetsPark, error) {
	var r PetsPark
	err := json.Unmarshal(data, &r)
	return r, err
}

func UnmarshalPetsPark2(data []byte) (PetsPark, error) {
	var r PetsPark
	err := jsoniter.Unmarshal(data, &r)
	return r, err
}

func (r *PetsPark) Marshal() ([]byte, error) {
	return json.Marshal(r)
}

func (r *PetsPark) Marshal2() ([]byte, error) {
	return jsoniter.Marshal(r)
}

type PetsParkElement struct {
	ID        float64    `json:"id"`
	Category  *Category  `json:"category,omitempty"`
	Name      *string    `json:"name,omitempty"`
	PhotoUrls []string   `json:"photoUrls"`
	Tags      []Category `json:"tags"`
	Status    Status     `json:"status"`
}

type Category struct {
	ID   int64  `json:"id"`
	Name string `json:"name"`
}

type Status string

const (
	Sold Status = "sold"
)

func main() {
	boolJSON := json.Valid(jsonDat)
	if !boolJSON {
		panic("Json is not Valid")
	}

}

func chekErr(err error) {
	if err != nil {
		panic(err)
	}
}

// сам json из Swagger
var jsonDat = []byte(`
[
  {
    "id": 9223372036854776000,
    "category": {
      "id": -1545904,
      "name": "fish"
    },
    "name": "nemo",
    "photoUrls": [
      "fugiat ex",
      "https://img.freepik.com/free-vector/one-goldfish-on-white-background_1308-73185.jpg"
    ],
    "tags": [
      {
        "id": 78174477,
        "name": "sint aliquip"
      },
      {
        "id": 34196593,
        "name": "tempor cillum incididunt exercitation veniam"
      }
    ],
    "status": "sold"
  },
  {
    "id": 9223372036854769000,
    "category": {
      "id": 0,
      "name": "string"
    },
    "name": "doggie",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 0,
        "name": "string"
      }
    ],
    "status": "sold"
  },
  {
    "id": 9223372036854776000,
    "category": {
      "id": 16225539,
      "name": "fish"
    },
    "name": "Petya_123",
    "photoUrls": [
      "esse deserunt exercitation non qui",
      "https://i.mycdn.me/i?r=AyH4iRPQ2q0otWIFepML2LxRdLpGogJJk_9WDkZTh0bGBQ"
    ],
    "tags": [
      {
        "id": 93211787,
        "name": "cat black"
      },
      {
        "id": 16690601,
        "name": "cupidatat s"
      }
    ],
    "status": "sold"
  },
  {
    "id": 76342797,
    "category": {
      "id": -51928180,
      "name": "consequat deserunt"
    },
    "name": "doggie",
    "photoUrls": [
      "proident consectetur dolor enim",
      "eiusmod ut ex"
    ],
    "tags": [
      {
        "id": -79491736,
        "name": "officia elit esse"
      },
      {
        "id": -95156536,
        "name": "consequat ullamco"
      }
    ],
    "status": "sold"
  },
  {
    "id": 13,
    "category": {
      "id": 0,
      "name": "cats"
    },
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 0,
        "name": "string"
      }
    ],
    "status": "sold"
  },
  {
    "id": 123456,
    "category": {
      "id": 0,
      "name": "string"
    },
    "name": "bird",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 0,
        "name": "string"
      }
    ],
    "status": "sold"
  },
  {
    "id": 607470813,
    "category": {
      "id": -62105314,
      "name": "est reprehenderit dolor ipsum"
    },
    "name": "executing from test",
    "photoUrls": [
      "sint quis elit et in",
      "ullamco Ut do deserunt mollit"
    ],
    "tags": [
      {
        "id": -21699130,
        "name": "ullamco qui"
      },
      {
        "id": 34531062,
        "name": "labore nostrud"
      }
    ],
    "status": "sold"
  },
  {
    "id": 2046,
    "category": {
      "id": 2046,
      "name": "dog"
    },
    "name": "Rocky",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 2046,
        "name": "beagle"
      }
    ],
    "status": "sold"
  },
  {
    "id": 1234567890,
    "category": {
      "id": 0,
      "name": "string"
    },
    "name": "doggie",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 0,
        "name": "string"
      }
    ],
    "status": "sold"
  },
  {
    "id": 169646791,
    "category": {
      "id": -62105314,
      "name": "est reprehenderit dolor ipsum"
    },
    "name": "executing from test",
    "photoUrls": [
      "sint quis elit et in",
      "ullamco Ut do deserunt mollit"
    ],
    "tags": [
      {
        "id": -21699130,
        "name": "ullamco qui"
      },
      {
        "id": 34531062,
        "name": "labore nostrud"
      }
    ],
    "status": "sold"
  },
  {
    "id": 741489934,
    "category": {
      "id": -62105314,
      "name": "est reprehenderit dolor ipsum"
    },
    "name": "executing from test",
    "photoUrls": [
      "sint quis elit et in",
      "ullamco Ut do deserunt mollit"
    ],
    "tags": [
      {
        "id": -21699130,
        "name": "ullamco qui"
      },
      {
        "id": 34531062,
        "name": "labore nostrud"
      }
    ],
    "status": "sold"
  },
  {
    "id": 680438414,
    "category": {
      "id": -62105314,
      "name": "est reprehenderit dolor ipsum"
    },
    "name": "executing from test",
    "photoUrls": [
      "sint quis elit et in",
      "ullamco Ut do deserunt mollit"
    ],
    "tags": [
      {
        "id": -21699130,
        "name": "ullamco qui"
      },
      {
        "id": 34531062,
        "name": "labore nostrud"
      }
    ],
    "status": "sold"
  },
  {
    "id": 9223372036854776000,
    "category": {
      "id": -1545904,
      "name": "fishs"
    },
    "name": "nemo fish",
    "photoUrls": [
      "fugiat ex",
      "https://img.freepik.com/free-vector/one-goldfish-on-white-background_1308-73185.jpg"
    ],
    "tags": [
      {
        "id": 78174477,
        "name": "sint aliquip"
      },
      {
        "id": 34196593,
        "name": "tempor cillum incididunt exercitation veniam"
      }
    ],
    "status": "sold"
  },
  {
    "id": 19961996,
    "category": {
      "id": 0,
      "name": "string"
    },
    "name": "numeAnimal2",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 0,
        "name": "string"
      }
    ],
    "status": "sold"
  },
  {
    "id": 20593238,
    "category": {
      "id": 96650357,
      "name": "ut aute"
    },
    "name": "doggie",
    "photoUrls": [
      "in deserunt sit qui elit",
      "do dolore"
    ],
    "tags": [
      {
        "id": 80489283,
        "name": "dolore elit aute Excepteur anim"
      },
      {
        "id": -29854588,
        "name": "enim"
      }
    ],
    "status": "sold"
  },
  {
    "id": 82983737,
    "category": {
      "id": -76414948,
      "name": "nulla cupidatat"
    },
    "name": "doggie",
    "photoUrls": [
      "in dolor voluptate irure",
      "ipsum"
    ],
    "tags": [
      {
        "id": -38281336,
        "name": "Ut qui"
      },
      {
        "id": 99117158,
        "name": "ut labore t"
      }
    ],
    "status": "sold"
  },
  {
    "id": 19288019,
    "photoUrls": [],
    "tags": [],
    "status": "sold"
  },
  {
    "id": 518,
    "category": {
      "id": 0,
      "name": "string"
    },
    "name": "mydogisgod",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 0,
        "name": "string"
      }
    ],
    "status": "sold"
  },
  {
    "id": 19288020,
    "photoUrls": [],
    "tags": [],
    "status": "sold"
  },
  {
    "id": 57436215795444,
    "category": {
      "id": 872545821,
      "name": "domestic dog"
    },
    "name": "Bim",
    "photoUrls": [
      "https://mobimg.b-cdn.net/v3/fetch/47/477a61586eda3cbc48901169b0fd6722.jpeg"
    ],
    "tags": [
      {
        "id": 2462039013,
        "name": "little dog"
      }
    ],
    "status": "sold"
  },
  {
    "id": 310,
    "category": {
      "id": 0,
      "name": "blacky"
    },
    "name": "doggie",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 310,
        "name": "string"
      }
    ],
    "status": "sold"
  },
  {
    "id": 9223372036854646000,
    "category": {
      "id": 0,
      "name": "string"
    },
    "name": "Cat",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 0,
        "name": "string"
      }
    ],
    "status": "sold"
  },
  {
    "id": 56331470,
    "category": {
      "id": 28262311,
      "name": "nostrud proident"
    },
    "name": "doggie",
    "photoUrls": [
      "sunt dolor veniam aliqua",
      "nulla anim ex"
    ],
    "tags": [
      {
        "id": 21515670,
        "name": "voluptate in elit ani"
      },
      {
        "id": -36858804,
        "name": "proident non deserunt"
      }
    ],
    "status": "sold"
  },
  {
    "id": 12084391,
    "category": {
      "id": -48583,
      "name": "cupidatat amet commodo"
    },
    "name": "doggie",
    "photoUrls": [
      "fugiat Ut eu",
      "exercitation nostr"
    ],
    "tags": [
      {
        "id": -27183415,
        "name": "e"
      },
      {
        "id": 86979382,
        "name": "in anim consectetur Lorem ut"
      }
    ],
    "status": "sold"
  },
  {
    "id": 334754,
    "category": {
      "id": 0,
      "name": "string"
    },
    "name": "butterfly_163",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 0,
        "name": "string"
      }
    ],
    "status": "sold"
  },
  {
    "id": 75559,
    "category": {
      "id": 0,
      "name": "string"
    },
    "name": "butterfly_694",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 0,
        "name": "string"
      }
    ],
    "status": "sold"
  },
  {
    "id": 16498,
    "category": {
      "id": 0,
      "name": "string"
    },
    "name": "butterfly_397",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 0,
        "name": "string"
      }
    ],
    "status": "sold"
  },
  {
    "id": 527696,
    "category": {
      "id": 0,
      "name": "string"
    },
    "name": "butterfly_619",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 0,
        "name": "string"
      }
    ],
    "status": "sold"
  },
  {
    "id": 91507602,
    "category": {
      "id": -95494262,
      "name": "sit ea"
    },
    "name": "doggie",
    "photoUrls": [
      "deserunt",
      "id sint et"
    ],
    "tags": [
      {
        "id": -54029842,
        "name": "ullamco"
      },
      {
        "id": 13198949,
        "name": "id"
      }
    ],
    "status": "sold"
  },
  {
    "id": 3903023,
    "category": {
      "id": -56789506,
      "name": "cillum p"
    },
    "name": "doggie",
    "photoUrls": [
      "aliquip Excepteur",
      "commodo dolore consequat eu"
    ],
    "tags": [
      {
        "id": 60937326,
        "name": "irure in"
      },
      {
        "id": 20218536,
        "name": "sint consectetur ad voluptate"
      }
    ],
    "status": "sold"
  },
  {
    "id": 3708840,
    "category": {
      "id": 9855812,
      "name": "Duis in commodo"
    },
    "name": "doggie",
    "photoUrls": [
      "minim id in sit",
      "est esse culpa nisi"
    ],
    "tags": [
      {
        "id": -89808508,
        "name": "sit"
      },
      {
        "id": -5238278,
        "name": "dolor in"
      }
    ],
    "status": "sold"
  },
  {
    "id": 24093935,
    "category": {
      "id": -6714225,
      "name": "do magna"
    },
    "name": "doggie",
    "photoUrls": [
      "aute",
      "velit"
    ],
    "tags": [
      {
        "id": 71561130,
        "name": "deserunt ut occaecat"
      },
      {
        "id": -1195842,
        "name": "nulla nostrud occaeca"
      }
    ],
    "status": "sold"
  },
  {
    "id": 12,
    "category": {
      "id": 0,
      "name": "dogs"
    },
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 0,
        "name": "string"
      }
    ],
    "status": "sold"
  },
  {
    "id": 74408294,
    "category": {
      "id": 70278030,
      "name": "in adipisicing"
    },
    "name": "doggie",
    "photoUrls": [
      "nostrud sunt eu occaecat elit",
      "esse aute non magna"
    ],
    "tags": [
      {
        "id": 81618844,
        "name": "id aliquip"
      },
      {
        "id": -96817599,
        "name": "ut elit esse"
      }
    ],
    "status": "sold"
  },
  {
    "id": 1303,
    "category": {
      "id": 0,
      "name": "dog"
    },
    "name": "Lana",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 0,
        "name": "string"
      }
    ],
    "status": "sold"
  }
]`)
