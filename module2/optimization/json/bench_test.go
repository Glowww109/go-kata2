package main

import "testing"

var (
	pets PetsPark
	err  error
	data []byte
)

func BenchmarkStandardJsonSERIALIZATION(b *testing.B) {
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		data, err = pets.Marshal()
		chekErr(err)
		_ = data
	}
}

func BenchmarkStandardJsonDESERIALIZATION(b *testing.B) {
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		pets, err = UnmarshalPetsPark(jsonDat)
		chekErr(err)
	}
}

func BenchmarkIterJsonSERIALIZATION(b *testing.B) {
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		data, err = pets.Marshal2()
		chekErr(err)
		_ = data
	}
}

func BenchmarkIterJsonDESERIALIZATION(b *testing.B) {
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		pets, err = UnmarshalPetsPark2(jsonDat)
		chekErr(err)
	}
}

//По итогу 4х бенчмарков, библиотека jsoniter:
//1.В сериализации медленнее стандартной почти на 53849%(0_0),
//хотя аллокация памяти практически такая же.
//2. В десиариализации ситуация наоборот, стандартная библиотека медленнее на 150%.
//По итогу данного теста можно сделать вывод, что для десиариализации однозначно
//лучше использовать jsoniter, а для сериализации лучше оставить стандартную библиотеку.
