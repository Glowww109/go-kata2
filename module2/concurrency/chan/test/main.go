package main

import (
	"crypto/rand"
	"encoding/binary"
	"fmt"
	"math/big"
	"sync"
)

func RandomGenerator() <-chan uint64 {
	c := make(chan uint64)
	go func() {
		rnds := make([]byte, 8)
		for {
			_, err := rand.Read(rnds)
			if err != nil {
				close(c)
				break
			}
			c <- binary.BigEndian.Uint64(rnds)
		}
	}()
	return c
}

func Aggregator(inputs ...<-chan uint64) <-chan uint64 {
	output := make(chan uint64)
	var wg sync.WaitGroup
	for _, in := range inputs {
		wg.Add(1)
		go func(in <-chan uint64) {
			defer wg.Done()
			for x := range in {
				output <- x
			}
		}(in)
	}
	go func() {
		wg.Wait()
		close(output)
	}()
	return output
}

func Divisor(input <-chan uint64, outputs ...chan<- uint64) {
	for _, out := range outputs {
		go func(out chan<- uint64) {
			for {
				out <- <-input
			}
		}(out)
	}
}

func Composer(inA, inB <-chan uint64) <-chan uint64 {
	output := make(chan uint64)
	go func() {
		for {
			a1, b, a2 := <-inA, <-inB, <-inA
			output <- a1 ^ b&a2
		}
	}()
	return output
}

func Duplicator(in <-chan uint64) (<-chan uint64, <-chan uint64) {
	outA, outB := make(chan uint64), make(chan uint64)
	go func() {
		for x := range in {
			outA <- x
			outB <- x
		}
	}()
	return outA, outB
}

func Calculator(in <-chan uint64, out chan uint64) <-chan uint64 {
	if out == nil {
		out = make(chan uint64)
	}
	go func() {
		for x := range in {
			out <- ^x
		}
	}()
	return out
}

func Filter0(input <-chan uint64, output chan uint64) <-chan uint64 {
	if output == nil {
		output = make(chan uint64)
	}
	go func() {
		bigInt := big.NewInt(0)
		for x := range input {
			bigInt.SetUint64(x)
			if bigInt.ProbablyPrime(1) {
				output <- x
			}
		}
	}()
	return output
}

func Filter(input <-chan uint64) <-chan uint64 {
	return Filter0(input, nil)
}

func Printer(input <-chan uint64) {
	counter := 0
	for x := range input {
		counter++
		fmt.Println(x)
		if counter >= 10000 {
			break
		}
	}
}

func main() {
	//	RandomGenerator()              //генератор случайных чисел, выводит заполненный канал
	//	Aggregator(RandomGenerator())  //объединяет несколько каналов в один
	//	Divisor(RandomGenerator(), ch) //записывает из первого значения во каждое из следующих
	//	Composer(ch, ch)               //объединение нескольких фрагментов в один
	//	Duplicator(ch)                 //один фрагмент данных дублируется в 2 потока
	//	Calculator(ch, ch)             //функция калькулятора, которая меняет знак у первого потока и пихает данные во второй поток
	//	Filter(ch)                     //функция, которая пропускает канал, только если у него записано простое число
	//	Printer(ch)                    //печатает то, что находится у нас в канале

	Printer(
		Filter(
			Calculator(
				RandomGenerator(), nil,
			),
		),
	)

	c1 := make(chan uint64, 100)
	Filter0(RandomGenerator(), c1)
	Filter0(RandomGenerator(), c1)
	Filter0(RandomGenerator(), c1)
	c2 := make(chan uint64, 100)
	Calculator(c1, c2)
	Calculator(c1, c2)
	Printer(c2)

}
