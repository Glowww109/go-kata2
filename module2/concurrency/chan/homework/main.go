package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

//Этот вариант на тесте тоже проходил, но его не стал включать в итог

//func joinChannels(chs ...<-chan int) chan int {
//	outCh := make(chan int)
//
//	var wg sync.WaitGroup
//
//	for i := 0; i < len(chs); i++ {
//		ch := chs[i]
//		go func(ch <-chan int) {
//			for value := range ch {
//				wg.Add(1)
//				outCh <- value
//				wg.Done()
//			}
//			wg.Wait()
//			close(outCh)
//		}(ch)
//	}
//	return outCh
//}

// решение на примере агрегатора из примера в уроках
func joinChannels(chs ...<-chan int) <-chan int {
	outputCh := make(chan int)
	var wg sync.WaitGroup

	for _, ch := range chs {
		wg.Add(1)
		go func(ch <-chan int) {
			defer wg.Done()
			for x := range ch {
				outputCh <- x
			}
		}(ch)
	}
	go func() {
		wg.Wait()
		close(outputCh)
	}()
	return outputCh
}

func generateData() chan int {
	out := make(chan int, 1000)

	go func() {
		defer close(out)
		for {
			select {
			case _, ok := <-out:
				if !ok {
					return
				}
			case out <- rand.Intn(100):
			}
		}
	}()

	return out
}

func main() {

	const timeOut = time.Second * 30

	ticker := time.NewTicker(time.Second)

	rand.Seed(time.Now().UnixNano())
	a := make(chan int)
	b := make(chan int)
	c := make(chan int)

	done := make(chan struct{}, 1) //канал/сообщение о закрытии всех каналов: a, b, c

	out := generateData()

	go func() {
		for num := range out {
			select {
			case <-done:
				close(a)
				fmt.Println("Канал а закрыт")
				return
			default:
				a <- num
			}
		}
	}()

	go func() {
		for num := range out {
			select {
			case <-done:
				close(b)
				fmt.Println("Канал б закрыт")
				return
			default:
				b <- num
			}
		}
	}()

	go func() {
		for num := range out {
			select {
			case <-done:
				close(c)
				fmt.Println("Канал в закрыт")
				return
			default:
				c <- num
			}
		}
	}()

	go func() {
		for {
			t := <-ticker.C
			fmt.Println(t)
		}
	}()

	go func() { //горутина для закрытия каналов a, b, c
		time.Sleep(timeOut)
		ticker.Stop()
		done <- struct{}{}
		close(done)

	}()

	mainChan := joinChannels(a, b, c)

	for num := range mainChan {
		//fmt.Println(num) //здесь убрал вывод, оставив только вывод тикера
		_ = num
	}
	time.Sleep(time.Second)
}
