package main

import (
	"fmt"
	"sync"

	"golang.org/x/sync/errgroup"
)

var mu sync.Mutex
var wg sync.WaitGroup

type Cache struct {
	data map[string]interface{}
	init bool
}

func NewCache() *Cache {
	return &Cache{
		data: make(map[string]interface{}, 100),
		init: true,
	}
}

func (c *Cache) Set(key string, v interface{}) error {
	if !c.init {
		return fmt.Errorf("cache isnt initialized")
	}
	//Тогда мьютекс здесь надо ставить
	mu.Lock()
	c.data[key] = v
	mu.Unlock()

	return nil
}

func (c *Cache) Get(key string) interface{} {
	//вот здесь сложно. Честно говоря, не понимаю, куда можно можно использовать мьютекс
	//Предположу, что тогда на всю функцию + return сделать по аналогии с Set
	//В 74 строке пример вызова метода Get. Но при проверке data race не возникает.

	//mu.Lock()
	if !c.init {
		return nil
	}
	v := c.data[key]
	//mu.Unlock()
	return v
}

func main() {
	cache := NewCache()
	keys := []string{
		"programming",
		"is",
		"so",
		"awesome",
		"write",
		"clean",
		"code",
		"use",
		"solid",
		"principles",
	}

	var eg errgroup.Group
	for i := range keys {
		idx := i
		eg.Go(func() error {
			return cache.Set(keys[idx], idx)
		})
	}

	//go func() {
	//	wg.Add(len(keys))
	//	for i := 0; i < len(keys); i++ {
	//		time.Sleep(time.Millisecond)
	//		fmt.Println(cache.Get(keys[i]))
	//		wg.Done()
	//	}
	//}()

	err := eg.Wait()
	if err != nil {
		panic(err)
	}

	wg.Wait()
	//для вывода карты
	fmt.Println(cache)

}
