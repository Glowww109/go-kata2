package main

import (
	"context"
	"fmt"
	"runtime"
	"sync"
	"time"
)

type key string

func main() {
	baseKnowledge()
	workerPool()
}
func baseKnowledge() {
	//контекст работает как матрёшки: создаём родительский контекст и уже в него создаём другие контексты

	fmt.Println(runtime.NumCPU())

	ctx := context.Background()
	fmt.Println(ctx)

	todo := context.TODO()
	fmt.Println(todo)

	withValue := context.WithValue(ctx, key("name"), "Petya")
	fmt.Println(withValue.Value("name"))

	withCancel, cancel0 := context.WithCancel(ctx)
	fmt.Println(withCancel.Err())
	cancel0()
	fmt.Println(withCancel.Err())

	withDeadline, cancel1 := context.WithDeadline(ctx, time.Now().Add(time.Second*3))
	defer cancel1()
	fmt.Println(withDeadline)
	fmt.Println(withDeadline.Err())
	fmt.Println(<-withDeadline.Done())
	fmt.Println(withDeadline.Err())

	withTimeout, cancel2 := context.WithTimeout(ctx, time.Second*2)
	defer cancel2()
	fmt.Println(<-withTimeout.Done())
}

// Далее рабочий пример. worker pool
func workerPool() {
	//работают оба варианта контекстов
	ctx, cancel := context.WithCancel(context.Background())
	//ctx, cancel := context.WithTimeout(context.Background(), time.Millisecond*20)
	defer cancel()

	wg := &sync.WaitGroup{}
	numberToProcess, processedNumbers := make(chan int, 5), make(chan int, 5)

	for i := 0; i <= runtime.NumCPU(); i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			worker(ctx, numberToProcess, processedNumbers)
		}()
	}

	go func() {
		wg.Wait()
		close(processedNumbers)
	}()

	go func() {
		for i := 0; i < 1000; i++ {
			//if i == 500 {
			//	//вот тут и применяет контекст
			//	cancel()
			//}
			numberToProcess <- i
		}
		close(numberToProcess)
	}()

	var counter int
	for resultValue := range processedNumbers {
		counter++
		fmt.Println(resultValue)
	}

	fmt.Println(counter)
}
func worker(ctx context.Context, toProcess <-chan int, processed chan<- int) {
	for {
		select {
		case <-ctx.Done():
			return
		case value, ok := <-toProcess:
			if !ok {
				return
			}
			time.Sleep(time.Millisecond)
			processed <- value * value
		}
	}
}
