package main

import (
	"context"
	"fmt"
	"math/rand"
	"sync"
	"time"
)

//Этот вариант на тесте тоже проходил, но его не стал включать в итог

//func joinChannels(chs ...<-chan int) chan int {
//	outCh := make(chan int)
//
//	var wg sync.WaitGroup
//
//	for i := 0; i < len(chs); i++ {
//		ch := chs[i]
//		go func(ch <-chan int) {
//			for value := range ch {
//				wg.Add(1)
//				outCh <- value
//				wg.Done()
//			}
//			wg.Wait()
//			close(outCh)
//		}(ch)
//	}
//	return outCh
//}

// решение на примере агрегатора из примера в уроках
func joinChannels(chs ...<-chan int) <-chan int {
	outputCh := make(chan int)
	var wg sync.WaitGroup

	for _, ch := range chs {
		wg.Add(1)
		go func(ch <-chan int) {
			defer wg.Done()
			for x := range ch {
				outputCh <- x
			}
		}(ch)
	}
	go func() {
		wg.Wait()
		close(outputCh)
	}()
	return outputCh
}

func generateData() chan int {
	out := make(chan int, 1000)

	go func() {
		defer close(out)
		for {
			select {
			case _, ok := <-out:
				if !ok {
					return
				}
			case out <- rand.Intn(100):
			}
		}
	}()

	return out
}

func main() {

	ticker := time.NewTicker(time.Second) //тикер оставил для отображения времени

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*30)
	defer cancel()

	rand.Seed(time.Now().UnixNano())
	a := make(chan int)
	b := make(chan int)
	c := make(chan int)

	out := generateData()

	go func() {
		for num := range out {
			select {
			case <-ctx.Done():
				close(a)
				go func() {
					time.Sleep(time.Millisecond * 100)
					fmt.Println("Канал a закрыт")
				}()
				return
			default:
				a <- num
			}
		}
	}()

	go func() {
		for num := range out {
			select {
			case <-ctx.Done():
				close(b)
				go func() {
					time.Sleep(time.Millisecond * 200)
					fmt.Println("Канал b закрыт")
				}()
				return
			default:
				b <- num
			}
		}
	}()

	go func() {
		for num := range out {
			select {
			case <-ctx.Done():
				close(c)
				go func() {
					time.Sleep(time.Millisecond * 300)
					fmt.Println("Канал c закрыт")
				}()
				return
			default:
				c <- num
			}
		}
	}()

	go func() { //горутина для вывода тикера и его завершения
		for {
			select {
			case t := <-ticker.C:
				fmt.Println(t)
			case <-ctx.Done():
				ticker.Stop()
			}
		}
	}()
	mainChan := joinChannels(a, b, c)

	for num := range mainChan {
		//fmt.Println(num) //здесь убрал вывод, оставив только вывод тикера
		_ = num
	}
	time.Sleep(time.Second)
}
