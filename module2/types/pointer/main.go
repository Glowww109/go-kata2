package main

import "fmt"

func newInt() *int {
	a := 3
	return &a
}

func main() {
	p0 := new(int)   // p0 указывает на 0 значение int
	fmt.Println(p0)  //здесь адрес в 16 ричной системе
	fmt.Println(*p0) //0

	//x это копия значения p0
	x := *p0
	//оба принимают значения х
	//x, *p1 и *p2 представляют одно значение
	p1, p2 := &x, &x
	fmt.Println(p1, p2, p0)
	fmt.Println(p1 == p2) //true
	fmt.Println(p0 == p1) //false
	p3 := p0              // <==> p3:=&(*p0) <==> p3 := p0
	//после этой операции p1, p2, p3 принимают одно значение
	fmt.Println(p0 == p3)
	*p0, *p1 = 123, 789
	fmt.Println(*p2, x, *p3) //789 789 123
	newInt()                 //здесь это просто для того, чтобы ошибка не выдавалась
}
