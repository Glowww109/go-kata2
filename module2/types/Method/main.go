package main

import "fmt"

type Square struct {
	Side int
}

func (s Square) Perimeter() {
	fmt.Printf("%T, %#v \n", s, s)
	fmt.Printf("Периметр фигуры: %d \n", s.Side*4)
}

func (s *Square) Scale(multiplier int) {
	fmt.Printf("%T, %#v \n", s, s)
	s.Side *= multiplier
}

func (s Square) WrongSquare(multyplyer int) {
	fmt.Printf("%T, %#v \n", s, s)
	s.Side *= multyplyer
	fmt.Printf("%T, %#v \n", s, s)
}

func main() {
	square := Square{Side: 4}
	pSquare := &square

	square2 := Square{Side: 2}

	square.Perimeter()
	square2.Perimeter()

	pSquare.Perimeter()
	pSquare.Scale(2)
	pSquare.Perimeter()

	square.WrongSquare(2)
	square.Perimeter()
}
