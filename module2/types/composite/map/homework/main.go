package main

import "fmt"

type Project struct {
	Name  string
	Stars int
}

func main() {
	projects := []Project{
		{
			Name:  "https://github.com/docker/compose",
			Stars: 27600,
		},
		{
			Name:  "https://github.com/karpathy/minGPT",
			Stars: 11746,
		},
		{
			Name:  "https://github.com/openai/openai-cookbook",
			Stars: 7577,
		},
		{
			Name:  "https://github.com/google/osv-scanner",
			Stars: 4095,
		},
		{
			Name:  "https://github.com/jart/blink",
			Stars: 3557,
		},
		{
			Name:  "https://github.com/microsoft/Web-Dev-For-Beginners",
			Stars: 64037,
		},
		{
			Name:  "https://github.com/musescore/MuseScore",
			Stars: 8940,
		},
		{
			Name:  "https://github.com/neonbjb/tortoise-tts",
			Stars: 3269,
		},
		{
			Name:  "https://github.com/openai/openai-python",
			Stars: 2002,
		},
		{
			Name:  "https://github.com/DataTalksClub/data-engineering-zoomcamp",
			Stars: 8123,
		},
		{
			Name:  "https://github.com/louislam/uptime-kuma",
			Stars: 27962,
		},
		{
			Name:  "https://github.com/AykutSarac/jsoncrack.com",
			Stars: 21400,
		},
		{
			Name:  "https://github.com/YauhenKavalchuk/interview-questions",
			Stars: 3084,
		},
	}
	projectMap := make(map[string]Project, len(projects))
	for _, project := range projects {
		projectMap[project.Name] = Project{
			Name:  project.Name,
			Stars: project.Stars,
		}
	}
	for _, eachProject := range projectMap {
		fmt.Println(eachProject)
	}
}
