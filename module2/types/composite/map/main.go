package main

import "fmt"

func main() {
	//простое объявление мапы
	var defaultmap map[int64]string

	fmt.Printf("Тип: %T, значение: %#v\n", defaultmap, defaultmap)
	fmt.Printf("Длина: %d\n", len(defaultmap))

	//Далее	создание через make
	mapByMake := make(map[string]string)
	fmt.Printf("Тип: %T, значение: %#v\n", mapByMake, mapByMake)

	//здесь мы добавляем в функции make вместе со вместимостью (3 в этом примере)
	mabByMake2 := make(map[string]string, 3)

	fmt.Printf("Тип: %T, значение: %#v\n", mabByMake2, mabByMake2)
	fmt.Printf("Длина: %d\n", len(mabByMake2))

	//Третий вариант создания мапы - с помощью литерала
	mapByLiteral := map[string]int{"Vasya": 18, "Dima": 20}
	fmt.Printf("Тип: %T, значение: %#v\n", mapByLiteral, mapByLiteral)
	fmt.Printf("Длина: %d\n", len(mapByLiteral))

	mapWithNew := *new(map[string]string)
	fmt.Printf("Тип: %T, значение: %#v\n", mapWithNew, mapWithNew)
	fmt.Printf("Длина: %d\n", len(mapWithNew))

	mapByMake["First"] = "Vasya"
	fmt.Printf("Тип: %T, значение: %#v\n", mapByMake, mapByMake)
	fmt.Printf("Длина: %d\n", len(mapByMake))

	mapByMake["First"] = "Petya"
	fmt.Printf("Тип: %T, значение: %#v\n", mapByMake, mapByMake)
	fmt.Printf("Длина: %d\n", len(mapByMake))

	fmt.Println(mapByLiteral["Vasya"])

	fmt.Println(mapByLiteral["Second"])

	value, ok := mapByLiteral["Second"]
	fmt.Printf("Тип: %T, значение: %#v\n", value, ok)

	delete(mapByMake, "First")
	fmt.Printf("Тип: %T, значение: %#v\n", mapByMake, mapByMake)
	fmt.Printf("Длина: %d\n", len(mapByMake))
}
