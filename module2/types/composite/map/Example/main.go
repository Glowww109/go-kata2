package main

import "fmt"

type User struct {
	Id   int64
	Name string
}

func main() {
	//простое объявление мапы
	var defaultmap map[int64]string

	fmt.Printf("Тип: %T, значение: %#v\n", defaultmap, defaultmap)
	fmt.Printf("Длина: %d\n", len(defaultmap))

	//Далее	создание через make
	mapByMake := make(map[string]string)
	fmt.Printf("Тип: %T, значение: %#v\n", mapByMake, mapByMake)

	//здесь мы добавляем в функции make вместе со вместимостью (3 в этом примере)
	mabByMake2 := make(map[string]string, 3)

	fmt.Printf("Тип: %T, значение: %#v\n", mabByMake2, mabByMake2)
	fmt.Printf("Длина: %d\n", len(mabByMake2))

	//Третий вариант создания мапы - с помощью литерала
	mapByLiteral := map[string]int{"Vasya": 18, "Dima": 20}
	fmt.Printf("Тип: %T, значение: %#v\n", mapByLiteral, mapByLiteral)
	fmt.Printf("Длина: %d\n", len(mapByLiteral))

	mapWithNew := *new(map[string]string)
	fmt.Printf("Тип: %T, значение: %#v\n", mapWithNew, mapWithNew)
	fmt.Printf("Длина: %d\n", len(mapWithNew))

	mapByMake["First"] = "Vasya"
	fmt.Printf("Тип: %T, значение: %#v\n", mapByMake, mapByMake)
	fmt.Printf("Длина: %d\n", len(mapByMake))

	mapByMake["First"] = "Petya"
	fmt.Printf("Тип: %T, значение: %#v\n", mapByMake, mapByMake)
	fmt.Printf("Длина: %d\n", len(mapByMake))

	fmt.Println(mapByLiteral["Vasya"])

	fmt.Println(mapByLiteral["Second"])

	value, ok := mapByLiteral["Second"]
	fmt.Printf("Тип: %T, значение: %#v\n", value, ok)

	delete(mapByMake, "First")
	fmt.Printf("Тип: %T, значение: %#v\n", mapByMake, mapByMake)
	fmt.Printf("Длина: %d\n", len(mapByMake))

	//map iteration
	marForIteration := map[string]int{"First": 1, "Second": 2, "Third": 3, "Fourth": 4}
	fmt.Println(marForIteration)

	for key, val := range marForIteration {
		fmt.Printf("Key is: %s Value is: %d\n", key, val)
	}

	//Пример уникальных значений

	users := []User{
		{
			Id:   1,
			Name: "Vasya",
		},
		{
			Id:   45,
			Name: "Petya",
		},
		{
			Id:   57,
			Name: "John",
		},
		{
			Id:   45,
			Name: "Petya",
		},
	}

	uniqUsers := make(map[int64]struct{}, len(users))

	for _, users := range users {
		if _, ok := uniqUsers[users.Id]; !ok {
			uniqUsers[users.Id] = struct{}{}
		}
	}
	fmt.Printf("Type: %T, Value: %#v\n", uniqUsers, uniqUsers)

	//быстрый поиск значения с помощью мапы

	usersMap := make(map[int64]User, len(users))
	for _, user := range users {
		if _, ok := usersMap[user.Id]; !ok {
			usersMap[user.Id] = user
		}
	}

	fmt.Println(findInSlice(57, users))
	fmt.Println(findIndMap(57, usersMap))
}

func findInSlice(id int64, users []User) *User {
	for _, user := range users {
		if user.Id == id {
			return &user
		}
	}
	return nil
}

func findIndMap(id int64, usersMap map[int64]User) *User {
	if user, ok := usersMap[id]; ok {
		return &user
	}
	return nil
}
