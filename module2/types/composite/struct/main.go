package main

import (
	"fmt"
	"unsafe"
)

type User struct { //объявление пользовательного типа
	Age      int
	Name     string
	Wallet   Wallet
	Location Location
}

type Location struct {
	Address string
	City    string
	index   string
}

type Wallet struct {
	Rur uint64
	Usd uint64
	Btc uint64
	Etc uint64
}

func main() {

	user := User{ //создаём переменную user с нашим типом User
		Age:  13,          //заполняем поля структуры
		Name: "Alexander", //заполняем поля структуры
	}
	fmt.Println(user)
	wallet := Wallet{
		Rur: 250000,
		Usd: 3500,
		Btc: 1,
		Etc: 4,
	}

	fmt.Println(wallet)
	fmt.Println("wallet allocates: ", unsafe.Sizeof(wallet), "bytes")
	user.Wallet = wallet
	fmt.Println(user)

	user2 := User{ //добавляем второго пользователя
		Age:  34,
		Name: "Anton",
		Wallet: Wallet{
			Rur: 144000,
			Usd: 8900,
			Btc: 55,
			Etc: 34,
		},
		Location: Location{
			Address: "Новатуттинская, 3-я ул., 13, к.2",
			City:    "Москва",
			index:   "108836",
		},
	}
	fmt.Println(user2)
}
