package main

//Задание: Удали первого и последнего пользователя, используя shift и pop из Slice tricks
//Ссылка - https://go.dev/play/p/FJ6_JP-sAFq

import "fmt"

type User struct {
	Name string
	Age  int
}

func main() {
	users := []User{
		{
			Name: "Alice",
			Age:  21,
		},
		{
			Name: "John",
			Age:  34,
		},
		{
			Name: "Alexander",
			Age:  45,
		},
		{
			Name: "Ivan",
			Age:  13,
		},
		{
			Name: "Denis",
			Age:  44,
		},
		{
			Name: "Mary",
			Age:  26,
		},
		{
			Name: "Rose",
			Age:  41,
		},
	}
	for i := 0; i <= len(users); i++ {
		//здесь, через условие только для первого значения, запускаем Pop front/shift(удаление первого пользователя)
		if i == 0 {
			users[i], users = users[0], users[1:]
		}
		//дальше, через условие только для последнего значения, запускаем Pop(удаление последнего пользователя)
		if i == len(users)-1 {
			users[i], users = users[len(users)-1], users[:len(users)-1]
		}
	}
	fmt.Println(users)
}
