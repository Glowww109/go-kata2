package main

//Задание: Удали пользователей возрастом выше 40 лет из слайса - https://go.dev/play/p/FJ6_JP-sAFq
//выведи результат в консоль

import "fmt"

type User struct {
	Name string
	Age  int
}

func main() {
	users := []User{
		{
			Name: "Alice",
			Age:  21,
		},
		{
			Name: "John",
			Age:  34,
		},
		{
			Name: "Alexander",
			Age:  45,
		},
		{
			Name: "Ivan",
			Age:  13,
		},
		{
			Name: "Denis",
			Age:  44,
		},
		{
			Name: "Mary",
			Age:  26,
		},
		{
			Name: "Rose",
			Age:  41,
		},
	}
	//Здесь запускаем цикл, чтобы найти нужное нам значение (Age)
	for i, user := range users {
		if user.Age >= 40 {
			//Далее мы удаляем лишний элемент при Age >= 40.
			//Делается это присвоением на место лишнего элемента следующего значения
			copy(users[i:], users[i+1:])
			//Далее мы добавляем крайнему элементу слайса пустое значение
			users[len(users)-1] = User{
				Name: "",
				Age:  0,
			}
			//Далее обозначим длину слайса, уменьшив итоговое значение на 1 (на 1 удалённый элемент)
			users = users[:len(users)-1]
		}
	}
	fmt.Println(users)
}
