package main

import "fmt"

//Задание: Измени программу так, чтобы изменения во втором слайсе не влияли на первый слайс,
//используя ресурсы по ссылке -https://go.dev/play/p/VRYlQ0v4Q9V

/*Не уверен в правильности решения. Для того чтобы изменения во втором слайсе не повлияли на первый, нам нужно
поменять ячейки памяти, на который они ссылаются.
Когда мы переполняем вместимость среза, то она удваивается. В этот момент слайсу выделяется новая ячейка памяти.
То есть слайс subUsers будет указывать на прошлую ячейку, а users - на новую.
*/

type User struct {
	Name string
	Age  int
}

func main() {

	users := []User{
		{
			Name: "Eva",
			Age:  13,
		},
		{
			Name: "Victor",
			Age:  28,
		},
		{
			Name: "Dex",
			Age:  34,
		},
		{
			Name: "Billy",
			Age:  21,
		},
		{
			Name: "Foster",
			Age:  29,
		},
	}
	subUsers := users[2:]

	//Само решение: добавляем ещё один элемент в слайс users перед началом функции editSecondSlice.
	//В этот момент слайсу users присваивается новый участок памяти, где он не пересекается со слайсом subUsers.
	users = append(users, User{
		Name: "NewUser",
		Age:  25,
	})

	editSecondSlice(subUsers)

	//Дальше вывод в консоль обоих слайсов для проверки выполнения условия
	fmt.Printf("len %d, cap %d\n", len(users), cap(users))
	fmt.Printf("len %d, cap %d\n", len(subUsers), cap(subUsers))
	fmt.Println(users)
	fmt.Println(subUsers)
}
func editSecondSlice(users []User) {
	for i := range users {
		users[i].Name = "unknown"
	}
}
