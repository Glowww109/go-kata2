package main

import "fmt"

type User struct { //объявление пользовательного типа
	Age    int
	Name   string
	Wallet Wallet
}

type Wallet struct {
	Rur uint64
	Usd uint64
	Btc uint64
	Eth uint64
}

func main() {
	testArray()
	rangeArray()
}

func testArray() {
	//создаём массив
	a := [...]int{34, 55, 89, 144}
	//выводим массив в терминал
	fmt.Println("original value", a)
	//меняем значение первого элемента
	a[0] = 21
	//выводим массив с изменённым первым элементом
	fmt.Println("change first element", a)
	//создаём копию массива
	b := a
	//меняем значение в массиве а
	a[0] = 233
	fmt.Println("original array", a)
	fmt.Println("original array", b)
}

func rangeArray() {
	users := [4]User{
		{
			Age:  8,
			Name: "John",
			Wallet: Wallet{
				Rur: 13,
				Usd: 1,
				Btc: 0,
				Eth: 0,
			},
		},
		{Age: 13,
			Name: "Kattie",
			Wallet: Wallet{
				Rur: 500,
				Usd: 3,
				Btc: 0,
				Eth: 0,
			},
		},
		{Age: 21,
			Name: "Doe",
			Wallet: Wallet{
				Rur: 0,
				Usd: 300,
				Btc: 1,
				Eth: 3,
			},
		},
		{Age: 34,
			Name: "Arnie",
			Wallet: Wallet{
				Rur: 987342,
				Usd: 34,
				Btc: 1,
				Eth: 3,
			},
		},
	}
	//Выводим пользователей старше 18 лет
	fmt.Println("пользователи старше 18 лет: ")
	for i := range users {
		if users[i].Age > 18 { //проверяем возраст больше 18
			fmt.Println(users[i]) //выводим в терминал, в случае удовлетворения условия
		}
	}
	fmt.Println("Пользователи у которых не нулевой баланс криптовалют: ")
	for i := range users {
		if users[i].Wallet.Btc > 0 || users[i].Wallet.Eth > 0 {
			fmt.Println(users[i])
		}
	}
}
