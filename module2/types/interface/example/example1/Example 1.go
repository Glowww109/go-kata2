package main

import "fmt"

type Runner interface {
	Run() string
}
type Swimmer interface {
	Swim() string
}
type Flyer interface {
	Fly() string
}

type Ducker interface {
	Runner
	Swimmer
	Flyer
}

type Human struct {
	Name string
}

type Duck struct {
	Name, Surname string
}

func (h Human) Run() string {
	return fmt.Sprintf("Человек %s Бегает", h.Name)
}

func (d Duck) Run() string {
	return "Утка бегает"
}
func (d Duck) Swim() string {
	return "Утка плавает"
}
func (d Duck) Fly() string {
	return "Утка летает"
}

func polymorphism(runner Runner) {
	fmt.Println(runner.Run())
}

func typeAssertion(runner Runner) {
	fmt.Printf("Type: %T Value: %#v\n", runner, runner)
	if human, ok := runner.(*Human); ok {
		fmt.Printf("Type: %T Value: %#v\n", human, human)

	}
	if duck, ok := runner.(*Duck); ok {
		fmt.Printf("Type: %T Value: %#v\n", duck, duck)
		fmt.Println(duck.Fly())
	}

	switch v := runner.(type) {
	case *Human:
		fmt.Println(v.Run())
	case *Duck:
		fmt.Println(v.Run())
	default:
		fmt.Printf("Type %T Value: %#v\n", v, v)
	}
}

func main() {
	var runner Runner
	fmt.Printf("Type %T Value %#v\n", runner, runner)
	john := &Human{Name: "John"}
	donald := &Duck{Name: "Donald", Surname: "Dick"}
	fmt.Println(john, donald)
	polymorphism(donald)
	typeAssertion(john)
}
