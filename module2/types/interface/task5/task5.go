// You can edit this code!
// Click here and start typing.
package main

/*
//Ошибкой задачи №5 было наличие разных методов: 1) у интерфейса 2) у структуры User
//Ошибка исправилась при приведении обоих методов к одному виду: GetName()

import "fmt"

	type User struct {
		ID   int
		Name string
	}

	func (u User) GetName() string {
		return u.Name
	}

	type Userer interface {
		GetName() string
	}
*/
func main() {
	// var i Userer
	// i = User{}
	// _ = i
	// fmt.Println("Success!")
}
