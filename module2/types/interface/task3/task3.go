// You can edit this code!
// Click here and start typing.
package main

//Решение: создаём слайс []Userer от интерфейса Userer. Дальше проходим по слайсу []User и переписываем через
//функцию append данные из слайса []User в новый слайс []Userer.
//+убрал обращение по ссылке в методе GetName к структуре User. Думаю, можно и дальше улучшить.

import (
	"fmt"
)

type User struct {
	ID   int
	Name string
}

func (u User) GetName() string {
	return u.Name
}

type Userer interface {
	GetName() string
}

func main() {
	u := []User{
		{
			ID:   34,
			Name: "Annet",
		},
		{
			ID:   55,
			Name: "John",
		},
		{
			ID:   89,
			Name: "Alex",
		},
	}
	var users []Userer
	for _, item := range u {
		users = append(users, item)
	}
	testUserName(users)
}

func testUserName(users []Userer) {
	for _, u := range users {
		fmt.Println(u.GetName())
	}
}
