package main

import (
	"fmt"
)

type MyInterface interface{}

func main() {
	var n *int
	fmt.Println(n == nil)
	//	test(n)
}

/*
func test(r interface{}) {
	//Не уверен в правильности решения.
	//Само решение: создал в функции test переменную n типа *int и значением nil.
	//Провёл type-switch, определил, что переданный тип интерфейса *int, после этого сравнил значение со значением
	//новой переменной (также, nil). В случае успеха вывод - Success!
	var n *int
	switch f := r.(type) {
	case string:
		fmt.Println("Fail!")
	case *int:
		if f == n {
			fmt.Println("Success!")
		}
	case nil:
		fmt.Println("Fail!")
	default:
		fmt.Println("Fail!")
	}
}
*/
