// You can edit this code!
// Click here and start typing.
package main

/*
	type User struct {
		ID   int
		Name string
	}

	func (u *User) GetName() string {
		return u.Name
	}

	type Userer interface {
		GetName() string
	}
*/
func main() {
	//	var u Userer
	//	u = &User{
	//		ID:   34,
	//		Name: "Annet",
	//	}
	//Здесь использовано * для того, чтобы переменная user стала типом *User.
	//	user := u.(*User)
	//здесь также добавляем * для правильной работы программы
	//	testUserName(*user)
}

/*
   func testUserName(u User) {
   	if u.GetName() == "Annet" {
   		fmt.Println("Success!")
   	}
   }
*/
