// You can edit this code!
// Click here and start typing.
package main

/*
import "fmt"

	type User struct {
		ID   int
		Name string
	}

	func (u *User) GetName() string {
		return u.Name
	}

	type Userer interface {
		GetName() string
	}
*/
func main() {
	//	var i Userer
	//	//здесь переменная i представляет собой адрес, так как в методе GetName получателем указан не User, a *User
	//	i = &User{}
	//	_ = i
	//	fmt.Println("Success!")
}
