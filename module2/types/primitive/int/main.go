package main

import (
	"fmt"
	"unsafe"
)

func main() {
	var uintNumber uint8 = 1 << 7    // делаем сдвиг на середину в uint8
	var from = int8(uintNumber)      //преобразовываем в int8
	uintNumber--                     //минусуем одно значение
	var to = int8(uintNumber)        //преобразовываем в int8
	fmt.Println(from, to)            //вывод в stdout(терминал)
	var uintNumber1 uint16 = 1 << 15 // делаем сдвиг на середину в uint8
	var from1 = int16(uintNumber1)   //преобразовываем в int8
	uintNumber1--                    //минусуем одно значение
	var to1 = int16(uintNumber1)     //преобразовываем в int8
	fmt.Println(from1, to1)          //вывод в stdout(терминал)
	//дальше начинаем продолжение, как из прошлого урока
	n := 112358132134
	fmt.Println("size is: ", unsafe.Sizeof(n), "bytes")
	typeint()
}

func typeint() {
	var numberint8 int8 = 1 << 1
	fmt.Println("left shift int8:", numberint8, "size", unsafe.Sizeof(numberint8), "bytes")
	numberint8 = (1 << 7) - 1
	fmt.Println("int 8 max value:", numberint8, "size", unsafe.Sizeof(numberint8), "bytes")
	var numberint16 int16 = (1 << 15) - 1
	fmt.Println("int16 max value:", numberint16, "size", unsafe.Sizeof(numberint16), "bytes")
	var numberint32 int32 = (1 << 31) - 1
	fmt.Println("int32 max value:", numberint32, "size", unsafe.Sizeof(numberint32), "bytes")
	var numberint64 int64 = (1 << 63) - 1
	fmt.Println("int64 max value:", numberint64, "size", unsafe.Sizeof(numberint64), "bytes")
	fmt.Println("===END type uint===")
}
