package main

import (
	"fmt"
	"unsafe"
)

func typebyte() {
	var b byte = 124
	fmt.Println("Размер в байтах:", unsafe.Sizeof(b), "bytes")
}

func main() {
	typebyte()
}
