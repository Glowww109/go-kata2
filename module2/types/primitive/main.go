package main

import (
	"fmt"
	"unsafe"
)

func main() {
	n := 112358132134
	fmt.Println("size is", unsafe.Sizeof(n), "bytes")
	typeint()
	typebyte()
	typeUint()
	typeBool()
}

func typebyte() {
	var b byte = 124
	fmt.Println("Размер в байтах:", unsafe.Sizeof(b), "bytes")
}

func typeint() {
	var numberint8 int8 = 1 << 1
	fmt.Println("left shift int8:", numberint8, "size", unsafe.Sizeof(numberint8), "bytes")
	numberint8 = (1 << 7) - 1
	fmt.Println("int 8 max value:", numberint8, "size", unsafe.Sizeof(numberint8), "bytes")
	var numberint16 int16 = (1 << 15) - 1
	fmt.Println("int16 max value:", numberint16, "size", unsafe.Sizeof(numberint16), "bytes")
	var numberint32 int32 = (1 << 31) - 1
	fmt.Println("int32 max value:", numberint32, "size", unsafe.Sizeof(numberint32), "bytes")
	var numberint64 int64 = (1 << 63) - 1
	fmt.Println("int64 max value:", numberint64, "size", unsafe.Sizeof(numberint64), "bytes")
	fmt.Println("===END type uint===")
}

func typeUint() {
	var numberUint8 uint8 = 1 << 1
	fmt.Println("left shift uint8:", numberUint8, "size", unsafe.Sizeof(numberUint8), "bytes")
	numberUint8 = (1 << 8) - 1
	fmt.Println("uint 8 max value:", numberUint8, "size", unsafe.Sizeof(numberUint8), "bytes")
	var numberUint16 uint16 = (1 << 16) - 1
	fmt.Println("uint16 max value:", numberUint16, "size", unsafe.Sizeof(numberUint16), "bytes")
	var numberUint32 uint32 = (1 << 32) - 1
	fmt.Println("uint32 max value:", numberUint32, "size", unsafe.Sizeof(numberUint32), "bytes")
	var numberUint64 uint64 = (1 << 64) - 1
	fmt.Println("uint64 max value:", numberUint64, "size", unsafe.Sizeof(numberUint64), "bytes")
	fmt.Println("===END type uint===")
}

func typeBool() {
	var b bool
	fmt.Println("Размер в байтах", unsafe.Sizeof(b))
}
