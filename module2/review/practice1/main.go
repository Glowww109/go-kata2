package main

import (
	"fmt"
	"math/rand"
	"sort"
	"time"
)

type Reverser interface {
	Reverse()
}

type Sorter interface {
	Sort()
}

type SorterReverser interface {
	Reverser
	Sorter
}

type Numbers []int

func (n Numbers) Sort() { // функция сортировки массива, использовать библиотеку sort.Slice

	sort.Slice(n, func(i, j int) bool { return n[i] < n[j] })
}

func (n Numbers) Reverse() { // функция переворачивания массива, перевернуть используя метод a, b = b, a
	for i := 0; i < len(n)/2; i++ {
		n[i], n[len(n)-i-1] = n[len(n)-i-1], n[i]
	}
}

func main() {
	var numbers Numbers
	numbers = AddNumbers(numbers) // исправить чтобы добавляло значения
	SortReverse(numbers)          // исправить чтобы сортировало и переворачивала массив
	fmt.Println(numbers)
}

func AddNumbers(s Numbers) Numbers {
	rand.Seed(time.Now().Unix())
	for i := 0; i < 100; i++ {
		s = append(s, rand.Intn(100))
	}
	return s
}

func SortReverse(sr SorterReverser) {
	sr.Sort()
	sr.Reverse()
}
