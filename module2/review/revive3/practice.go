package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

func RandNumbers(length, max int) []int {
	var s []int
	rand.Seed(time.Now().UnixNano())
	for i := 0; i < length; i++ {
		s = append(s, rand.Intn(max))
	}
	return s
}

func writeToChan(ch chan<- int) {
	defer close(ch)
	for _, v := range RandNumbers(1, 100) {
		ch <- v
	}
}

func mergeChan(ch ...chan int) chan int {
	//Задача: сделать объединение каналов

	mergedChan := make(chan int)
	var wg sync.WaitGroup

	for _, in := range ch {
		wg.Add(1)
		go func(in <-chan int) {
			defer wg.Done()
			for x := range in {
				mergedChan <- x
			}
		}(in)
	}
	go func() {
		wg.Wait()
		close(mergedChan)
	}()
	return mergedChan
}

func main() {
	ch1 := make(chan int)
	ch2 := make(chan int)
	ch3 := make(chan int)
	ch4 := make(chan int)

	mergedChan := mergeChan(ch1, ch2, ch3, ch4)
	go writeToChan(ch1)
	go writeToChan(ch2)
	go writeToChan(ch3)
	go writeToChan(ch4)

	for {
		val, ok := <-mergedChan
		if !ok {
			break
		}
		fmt.Println(val)
	}
}
