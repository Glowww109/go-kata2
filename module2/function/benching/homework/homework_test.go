package main

import (
	"testing"
)

func BenchmarkSample(b *testing.B) {
	users := genUsers()
	products := genProducts()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		MapUserProducts(users, products)
	}
}

func BenchmarkSample2(b *testing.B) {
	users := genUsers()
	products := genProducts()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		MapUserProducts2(users, products)
	}
}
