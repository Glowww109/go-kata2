package main

//Не уверен, что будет более правильно, привожу 2 решения.

import (
	"fmt"
	"testing"
	"unicode"
)

func TestGreet(t *testing.T) {
	type args struct {
		name string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "english name",
			args: args{name: "John"},
			want: "Hello John, you welcome!",
		},
		{
			name: "russian name",
			args: args{name: "Боря"},
			want: "Привет Боря, добро пожаловать!",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			//здесь для 1 решения добавляем ещё 1 аргумент в функцию
			//if got := Greet(tt.args.name, tt.name); got != tt.want {
			if got := Greet(tt.args.name); got != tt.want {
				t.Errorf("Greet() = %v, want %v", got, tt.want)
			}
		})
	}
}

/*
1 Решение: добавить в Greet ещё одну переменную: testName(tests.name). Функция получает 2 переменные на вход и
по ним определяет какое именно сообщение передавать

	func Greet(name string, testName string) string {
		switch testName {
		case "english name":
			name = fmt.Sprintf("Hello %s, you welcome!", name)
		case "russian name":
			name = fmt.Sprintf("Привет %s, добро пожаловать!", name)
		}
		return name
	}
*/
//______________________________________________________________________________________________________________
//2 решение: определить нет ли в переданной строке кириллицы. Если есть, возвращаем значение для русского имени.
//Если нет - для английского
func Greet(name string) string {
	var russianLang bool
	for _, russian := range name {
		//проверяем символы юникода на кириллицу в переданной строке имени
		if unicode.Is(unicode.Cyrillic, russian) {
			russianLang = true
		}
	}
	switch russianLang {
	case true:
		name = fmt.Sprintf("Привет %s, добро пожаловать!", name)
	case false:
		name = fmt.Sprintf("Hello %s, you welcome!", name)
	}
	return name
}
