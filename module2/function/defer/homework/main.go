package main

import (
	"errors"
	"fmt"
)

type MergeDictsJob struct {
	Dicts      []map[string]string
	Merged     map[string]string
	IsFinished bool
}

// errors
var (
	errNotEnoughDicts = errors.New("at least 2 dictionaries are required")
	errNilDict        = errors.New("nil dictionary")
	//errNotEnoughDictsAndNilDict - нам нужен, если будет и errNotEnoughDicts и errNilDict
	errNotEnoughDictsAndNilDict = errors.New("at least 2 dictionaries are required + nil dictionary")
	//err - переменная, в которую мы будем записывать значение нужной ошибки из верхних трёх
	err = errors.New("")
	//count - нам нужен, чтобы сосчитать количество словарей в Dicts. Увеличивается на 1
	//в каждой итерации цикла строки 40
	count int
)

func main() {
	//exampleMergeDictJob - Пример структуры MergeDictJob, которая передаётся в функцию ExecuteMergeDictsJob
	exampleMergeDictJob := MergeDictsJob{
		Dicts:      []map[string]string{{"a": "b"}, nil, {"c": "d"}},
		Merged:     nil,
		IsFinished: false,
	}
	finalMergeDictJob, err := ExecuteMergeDictsJob(&exampleMergeDictJob)
	defer fmt.Println(err)
	defer fmt.Println(finalMergeDictJob)

}
func ExecuteMergeDictsJob(job *MergeDictsJob) (*MergeDictsJob, error) {
	for _, item := range job.Dicts {
		//Проверка на словарь со значением nil
		if item == nil {
			err = errNilDict
		}
		count += 1
		for key, value := range item {
			if item == nil {
				err = errNilDict
			}
			//здесь создаём карту, в которую будем записывать ключ/значение если она nil
			if job.Merged == nil {
				job.Merged = make(map[string]string)
			}
			//здесь мы записываем каждые ключ и значение Dicts в результирующую мапу Merged
			job.Merged[key] = value
		}
	}
	if count < 2 {
		if err == errNilDict {
			err = errNotEnoughDictsAndNilDict
		} else {
			err = errNotEnoughDicts
		}
	}
	//IsFinished ставим значение true
	job.IsFinished = true
	return job, err
}
