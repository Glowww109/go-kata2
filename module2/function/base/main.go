package main

import "fmt"

func Concat(sep string, tokens ...string) (r string) {
	for i, t := range tokens {
		if i != 0 {
			r += sep
		}
		r += t
	}
	return
}

func main() {
	tokens := []string{"GO", "C", "Rust"}
	//manner 1
	langA := Concat(",", tokens...)
	//manner 2
	langB := Concat(",", "GO", "C", "Rust")
	fmt.Println(langA)
	fmt.Println(langB)
	fmt.Println(langA == langB)

	/*
	   import "fmt"

	   func Sum(values ...int64) (sum int64) {
	   	//тип values - это int64
	   	sum = 0
	   	for _, v := range values {
	   		sum += v
	   	}
	   	return
	   }

	   func main() {
	   	a0 := Sum()
	   	a1 := Sum(2)
	   	a3 := Sum(2, 3, 5)
	   	//The above three lines are equivalent to
	   	//following three respected lines.
	   	b0 := Sum([]int64{}...) // <=> Sum(nil...)
	   	b1 := Sum([]int64{2}...)
	   	b3 := Sum([]int64{2, 3, 5}...)
	   	fmt.Println(a0, a1, a3)
	   	fmt.Println(b0, b1, b3)
	*/

}
