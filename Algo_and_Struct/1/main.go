package main

import (
	"fmt"
	"time"
)

func On(n int) {
	timer := time.Now()
	count := 0
	for it := 0; it < n; it++ {
		count++
	}
	fmt.Println("On(n):", time.Since(timer).Milliseconds())
}

func On2(n int) {
	timer := time.Now()
	count := 0
	for i := 0; i < n; i++ {
		for j := 0; j < n; j++ {
			count++
		}
	}
	fmt.Println("On2(n):", time.Since(timer).Milliseconds())
}

func logN(n int) {
	timer := time.Now()
	count := 0
	i := 1
	for i < n {
		count += 1
		i = i * 3
	}
	fmt.Println("logN(n):", time.Since(timer).Milliseconds())
}

func On_v2(n int) {
	timer := time.Now()
	count := 0
	for i := 0; i < n; i++ {
		count++
	}
	for i := 0; i < n; i++ {
		count++
	}
	fmt.Println("On(n):", time.Since(timer).Milliseconds())
}

func On_v3(n int) {
	timer := time.Now()
	k := 0
	count := 0
	for i := 0; i < n; i++ {
		for ; k < n; k++ {
			count += 1
		}
	}
	fmt.Println("On(n):", time.Since(timer).Milliseconds())
}

func main() {
	N := 10000
	On(N)
	On_v2(N)
	On_v3(N)
	On2(N)
	logN(N)

}
