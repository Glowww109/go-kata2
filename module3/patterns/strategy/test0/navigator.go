package main

type Navigator struct {
	Strategy
}

func (nav *Navigator) SetStrategy(strategy Strategy) {
	nav.Strategy = strategy
}
