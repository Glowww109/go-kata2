package main

import "fmt"

type PublicTransportStrategy struct {
}

func (road *PublicTransportStrategy) Route(startPoint, endPoint int) {
	avgSpeed := 40
	total := endPoint - startPoint
	totalTime := total * 40
	fmt.Printf("Public Transport A:[%d] to B:[%d] Avr speed [%d] Total: [%d] Total time: [%d] min\n", startPoint, endPoint, avgSpeed, total, totalTime)
}
