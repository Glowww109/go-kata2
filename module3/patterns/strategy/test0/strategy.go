package main

type Strategy interface {
	Route(startPoint, endPoint int)
}
