package main

import "fmt"

type WalkStrategy struct {
}

func (road *WalkStrategy) Route(startPoint, endPoint int) {
	avgSpeed := 4
	total := endPoint - startPoint
	totalTime := total * 60
	fmt.Printf("Walk A:[%d] to B:[%d] Avr speed [%d] Total: [%d] Total time: [%d] min\n",
		startPoint, endPoint, avgSpeed, total, totalTime)
}
