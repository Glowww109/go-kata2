package main

import "fmt"

var (
	bank = Bank{
		Name: "Банк",
		Card: []Card{},
	}
	card1 = Card{
		Name:    "CRD - 1",
		Bank:    &bank,
		Balance: 200,
	}
	card2 = Card{
		Name:    "CRD - 2",
		Bank:    &bank,
		Balance: 5,
	}
	user1 = User{
		Name: "Покупатель 1",
		Card: &card1,
	}
	user2 = User{
		Name: "Покупатель 2",
		Card: &card2,
	}
	prod = Product{
		Name:  "Сыр",
		Price: 150,
	}
	shop = Shop{
		Name: "SHOP",
		Products: []Product{
			prod,
		},
	}
)

func main() {
	fmt.Println("[Банк] выпуск карт")
	bank.Card = append(bank.Card, card1, card2)
	fmt.Printf("%s пришёл в магазин", user1.Name)
	err := shop.Sell(user1, prod.Name)
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	fmt.Printf("%s пришёл в магазин", user2.Name)
	err = shop.Sell(user2, prod.Name)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

}
