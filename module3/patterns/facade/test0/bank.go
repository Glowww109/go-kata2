package main

import (
	"errors"
	"fmt"
	"time"
)

type Bank struct {
	Name string
	Card []Card
}

func (b *Bank) CheckBalance(cardNumber string) error {
	fmt.Printf("[Банк] получение остатка по карте %s\n", cardNumber)
	time.Sleep(time.Second * 3)

	for _, card := range b.Card {
		if card.Name != cardNumber {
			continue
		}
		if card.Balance <= 0 {
			return errors.New("[Банк] недостаточно средств")
		}
	}
	fmt.Println("[Банк] Остаток положительный")
	return nil

}
