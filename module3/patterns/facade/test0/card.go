package main

import (
	"fmt"
	"time"
)

type Card struct {
	Name    string
	Bank    *Bank
	Balance float64
}

func (card *Card) CheckBalance() error {
	fmt.Println("[Карта] запрос в банк для проверки остатка")
	time.Sleep(time.Second * 2)
	return card.Bank.CheckBalance(card.Name)
}
