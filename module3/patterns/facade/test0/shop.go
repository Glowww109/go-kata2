package main

import (
	"errors"
	"fmt"
	"time"
) //В этом примере фасад - это функция Sell

// магазин к которому мы делаем платежную
type Shop struct {
	Name     string
	Products []Product
}

func (s *Shop) Sell(user User, product string) error {
	fmt.Println("[Магазин] Запрос к пользователю для получения остатков по карте")
	time.Sleep(time.Second * 1)
	err := user.Card.CheckBalance()
	if err != nil {
		return err
	}
	fmt.Printf("[Магазин] Проверка - может ли [%s] купить товар \n", user.Name)
	time.Sleep(time.Second * 2)
	for _, prod := range s.Products {
		if prod.Name != product {
			continue
		}
		if prod.Price > user.GetBalance() {
			return errors.New("[Магазин] недостаточно средств для покупки")
		}
		fmt.Printf("[Магазин] Товар [%s]- куплен\n", prod.Name)
	}
	return nil
}
