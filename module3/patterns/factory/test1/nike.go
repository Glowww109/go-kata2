package main

type Nike struct {
}

func (a *Nike) makeShoe() IShoe {
	return &AdidasShoe{
		Shoe{
			logo: "nike",
			size: 14,
		},
	}
}

func (a *Nike) makeShirt() IShirt {
	return &AdidasShirt{
		Shirt{
			logo: "nike",
			size: 14,
		},
	}
}
