package main

import "fmt"

type AsusComputer struct {
	Memory int
	Cpu    int
}

func (pc *AsusComputer) PrintDetails() {
	fmt.Printf("[Asus] Pc Cpu: [%d], Memory [%d]\n", pc.Cpu, pc.Memory)
}
