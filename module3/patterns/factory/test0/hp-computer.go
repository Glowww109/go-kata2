package main

import "fmt"

type HpComputer struct {
	Memory int
	Cpu    int
}

func (pc *HpComputer) PrintDetails() {
	fmt.Printf("[Asus] Pc Cpu: [%d], Memory [%d]\n", pc.Cpu, pc.Memory)
}
