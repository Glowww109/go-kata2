package main

import "fmt"

var (
	brands = []string{Asus, Hp, "dell"}
)

func main() {
	for _, brand := range brands {
		factory, err := GetFactory(brand)
		if err != nil {
			fmt.Println(err.Error())
			continue
		}
		monitor := factory.GetMonitor()
		monitor.PrintDetails()
		computer := factory.GetComputer()
		computer.PrintDetails()
	}
}
