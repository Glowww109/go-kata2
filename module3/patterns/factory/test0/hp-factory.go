package main

type HpFactory struct {
}

func (h *HpFactory) GetComputer() Computer {
	return &HpComputer{
		Memory: 8,
		Cpu:    4,
	}
}

func (h *HpFactory) GetMonitor() Monitor {
	return &HpMonitor{
		Size: 24,
	}
}
