package main

type AsusFactory struct {
}

func (a *AsusFactory) GetComputer() Computer {
	return &AsusComputer{
		Memory: 16,
		Cpu:    8,
	}
}

func (a *AsusFactory) GetMonitor() Monitor {
	return &AsusMonitor{
		Size: 32,
	}
}
