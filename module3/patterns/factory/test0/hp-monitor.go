package main

import "fmt"

type HpMonitor struct {
	Size int
}

func (pc *HpMonitor) PrintDetails() {
	fmt.Printf("[Asus] Monitor Size: [%d]\n", pc.Size)
}
