package main

import "fmt"

type multiplayer struct {
}

func (m *multiplayer) doMultiply(x, y int) int {
	fmt.Printf("Происходит умножение %v на %v\t", x, y)
	x = x * y
	fmt.Printf("Результат: %v\t", x)
	return x
}
