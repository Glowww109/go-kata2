package main

//идея: у клиента есть метод, позволяющий умножать числа, но ему нужно подключить сервис, который СКЛАДЫВАЕТ числа.

import "fmt"

type Client struct {
}

func (c *Client) doClientMultiply(com computer) {
	x := 5
	y := 10
	fmt.Printf("Клиент умножает %v на %v \t", x, y)

	com.doMultiply(x, y)
}
