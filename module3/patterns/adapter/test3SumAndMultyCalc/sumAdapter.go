package main

import "fmt"

type SumAdapter struct {
	summariser *summariser
}

func (sum *SumAdapter) doMultiply(x, y int) int {
	fmt.Println("Адаптер конвертирует операцию из умножения в сложение")
	fmt.Println("результат: ", sum.summariser.doSum(x, y))
	return sum.summariser.doSum(x, y)
}
