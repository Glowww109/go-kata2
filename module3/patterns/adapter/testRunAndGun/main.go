package main

import "fmt"

func main() {
	Clint := &client{}
	ClintRun := &run{}
	ClintGun := &gun{}
	ClintReload := &adapterRunAndGun{
		gun: ClintGun,
	}

	Clint.RunAndGun(ClintRun)

	Clint.RunAndGun(ClintReload)

	for i := 0; i <= 15; i++ {
		Clint.RunAndGun(ClintReload)
		d := i % 3
		if d == 0 {
			fmt.Println(i/3, " человек убит(о)")
		}
		if i == 12 {
			fmt.Println("Фатальный промах! Клинт прячется в бочку")
		}
	}
	fmt.Println("Клинт остаётся последним живым человек в салуне... и на земле")
}
