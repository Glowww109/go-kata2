package main

import "fmt"

type adapterRunAndGun struct {
	gun *gun
}

func (a *adapterRunAndGun) Run() {
	fmt.Println("Клинт перезаряжается")
	a.gun.gunShot()
}
