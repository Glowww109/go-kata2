package main

type Service interface {
	GetData(user string) ([]string, error)
}
