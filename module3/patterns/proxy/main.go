package main

import "fmt"

var (
	admin = "administrator"
	user  = "user"
	users = map[string]bool{
		"administrator": true,
		"user":          false,
	}
)

func main() {
	proxy := ProxyDataBase{
		Users: users,
		Db:    &Database{},
	}
	AdminData, err := proxy.GetData(admin)
	fmt.Printf("From [%s] Data [%v] Err [%v]\n", admin, AdminData, err)
	UserData, err := proxy.GetData(user)
	fmt.Printf("From [%s] Data [%v] Err [%v]\n", user, UserData, err)
}
