package main

import (
	"errors"
	"fmt"
	"time"
)

// Создать структуру Post
type Post struct {
	body        string
	publishDate int64
	next        *Post
}

// Создать структуру Feed
type Feed struct {
	length int
	start  *Post
	end    *Post
}

// Реализовать метод Append для структуры Feed
func (f *Feed) Append(newPost *Post) {
	if f.length == 0 {
		f.start = newPost
		f.end = newPost
	} else {
		lastPost := f.end
		lastPost.next = newPost
		f.end = newPost
	}
	f.length++
}

// Реализовать метод Remove для структуры Feed
func (f *Feed) Remove(publishDate int64) error {
	if f.length == 0 {
		err := errors.New("нет ни одного поста")
		return err
	}
	var previousPost *Post
	currentPost := f.start

	for publishDate != currentPost.publishDate {
		if currentPost.next == nil {
			err := errors.New("нет подходящего поста для удаления")
			return err
		}
		previousPost = currentPost
		currentPost = currentPost.next
	}
	previousPost.next = currentPost.next

	f.length--
	return nil
}

// Реализовать метод Insert для структуры Feed
func (f *Feed) Insert(newPost *Post) {
	if f.length == 0 {
		f.start = newPost
	} else {
		var previousPost *Post
		currentPost := f.start

		for currentPost.publishDate < newPost.publishDate {
			previousPost = currentPost
			currentPost = previousPost.next
		}

		previousPost.next = newPost
		newPost.next = currentPost
	}
	f.length++
}

// Реализовать метод Inspect для структуры Feed
func (f *Feed) Inspect() {
	if f.length == 0 {
		fmt.Println("Feed is empty")
	}
	fmt.Printf("Ниже приведена cчиталочка, в которой %d строф:\n\n", f.length)

	index := 0
	currentPost := f.start

	for index < f.length {
		fmt.Println(currentPost.body)
		currentPost = currentPost.next
		index++
	}
	fmt.Println()
}

// Написать функцию main, которая создает несколько постов, добавляет их в поток,
// а затем выводит информацию о потоке с помощью метода Inspect
func main() {
	var timeNow = time.Now().UnixNano()

	childrenRhyme := &Feed{}
	p1 := &Post{
		body:        "Жаба прыгала, скакала,",
		publishDate: timeNow + 100,
	}
	p2 := &Post{
		body:        "Чуть в болото не упала.",
		publishDate: timeNow + 200,
	}
	p3 := &Post{
		body:        "Из болота вышел дед,",
		publishDate: timeNow + 300,
	}
	p4 := &Post{
		body:        "Двести восемьдесят лет.",
		publishDate: timeNow + 400,
	}
	p5 := &Post{
		body:        "Нёс он травы и цветы.",
		publishDate: timeNow + 500,
	}
	p6 := &Post{
		body:        "Выходи из круга ты.",
		publishDate: timeNow + 600,
	}

	childrenRhyme.Append(p1)
	childrenRhyme.Append(p2)
	childrenRhyme.Append(p3)
	childrenRhyme.Append(p4)
	childrenRhyme.Append(p5)
	childrenRhyme.Append(p6)

	childrenRhyme.Inspect()

	fmt.Printf("Далее в текст будет добавлена ошибка через метод Insert\n\n")

	wrongPost := &Post{
		body:        "А вот и ошибка",
		publishDate: timeNow + 450,
	}
	childrenRhyme.Insert(wrongPost)

	childrenRhyme.Inspect()
}
