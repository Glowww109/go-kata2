package main

import (
	"math/rand"
	"testing"
	"time"
)

func generateData(n, count, maxValue int) [][]int {
	var data [][]int
	rand.Seed(time.Now().UnixNano())
	for i := 0; i < n; i++ {
		var cycleData []int
		for j := 0; j < count; j++ {
			cycleData = append(cycleData, rand.Intn(maxValue))
		}
		data = append(data, cycleData)
	}
	return data
}

func BenchmarkBubbleSort(b *testing.B) {
	data := generateData(b.N, 1000, 5000)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		BubbleSort(data[i])
	}
}

func BenchmarkBubbleSort2(b *testing.B) {
	data := []int{1, 1, 2, 3, 5, 8, 13, 21, 34, 55}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		BubbleSort(data)
	}
}
