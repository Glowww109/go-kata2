package main

import "fmt"

func BubbleSortAverage(data []int) []int {
	swapped := true //создаём переменную для проверки обмена местами в цикле
	i := 1          //выносим переменную отвечающую за изменение длины сортируемой части массива
	for swapped {
		swapped = false                    //по умолчанию говорим, что обмена в цикле не было
		for j := 0; j < len(data)-i; j++ { //len(data)-i не тратим время на отсортированную часть
			if data[j] > data[j+1] {
				data[j], data[j+1] = data[j+1], data[j]
				swapped = true //ставим swapped в true, если значения поменялись местами\
				fmt.Println(data)
			}
		}
		fmt.Println("-------------------------------")
		i++ //инкрементируем индекс для пропуска отсортированной части
	}
	return data
}

func main() {
	testData1 := []int{10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0}
	fmt.Println("Final", BubbleSortAverage(testData1))
}
