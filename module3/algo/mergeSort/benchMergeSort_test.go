package main

import (
	"math/rand"
	"testing"
	"time"
)

func generateData(n, count, maxValue int) [][]int {
	var data [][]int
	rand.Seed(time.Now().UnixNano())
	for i := 0; i < n; i++ {
		var cycleData []int
		for j := 0; j < count; j++ {
			cycleData = append(cycleData, rand.Intn(maxValue))
		}
		data = append(data, cycleData)
	}
	return data
}

func BenchmarkMergeSort(b *testing.B) {
	dataSet := generateData(b.N, 1000, 5000)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		mergeSort(dataSet[i])
	}
}
