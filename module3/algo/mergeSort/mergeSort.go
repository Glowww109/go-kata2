package main

func mergeSort(items []int) []int {
	if len(items) < 2 {
		return items
	}
	first := mergeSort(items[:len(items)/2])
	second := mergeSort(items[len(items)/2:])

	return merge(first, second)
}

func merge(a, b []int) []int {
	//fmt.Println("a - ", a)
	//fmt.Println("b - ", b)
	final := []int{}
	i := 0
	j := 0

	for i < len(a) && j < len(b) {
		if a[i] < b[j] {
			final = append(final, a[i])
			i++
			//fmt.Println("первый цикл, а", final)
		} else {
			final = append(final, b[j])
			j++
			//fmt.Println("второй цикл, б", final)
		}
	}
	for ; i < len(a); i++ {
		final = append(final, a[i])
		//fmt.Println("общий цикл, a", final)
	}
	for ; j < len(b); j++ {
		final = append(final, b[j])
		//fmt.Println("общий цикл, б", final)
	}
	//fmt.Println("итог: ", final)
	return final
}

func main() {
	date := []int{3, 5, 2, 1, 4, 7}
	mergeSort(date)
}
