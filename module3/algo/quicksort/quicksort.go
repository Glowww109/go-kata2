package quicksort

import (
	"math/rand"
)

func Quicksort(arr []int) []int {
	if len(arr) <= 1 {
		return arr
	}
	median := arr[rand.Intn(len(arr))] // находим медиану выбирая случайное число

	lowPart := make([]int, 0, len(arr))    //сюда записываем числа меньше числа в медиане(pivot)
	highPart := make([]int, 0, len(arr))   //сюда записываем числа больше числа в медиане(pivot)
	middlePart := make([]int, 0, len(arr)) //здесь будут храниться числа равные медиане(pivot)

	for _, item := range arr {
		switch {
		case item < median:
			lowPart = append(lowPart, item) //запись чисел меньше медианы
		case item == median:
			middlePart = append(middlePart, item) //запись чисел равных медиане
		case item > median:
			highPart = append(highPart, item) //запись чисел больше медианы
		}
	}
	lowPart = Quicksort(lowPart)
	highPart = Quicksort(highPart)

	//распаковываем среднюю часть после всех чисел
	lowPart = append(lowPart, middlePart...)
	//далее распаковываем оставшуюся часть
	lowPart = append(lowPart, highPart...)
	return lowPart
}
