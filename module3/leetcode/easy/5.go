package easy

//1688. Count of Matches in Tournament
import "fmt"

func numberOfMatches(n int) int {
	var matchCount int
	if n == 1 {
		return matchCount
	}
	for {
		switch even(n) {
		case true:
			if n == 2 {
				matchCount++
				return matchCount
			}
			matchCount = matchCount + (n / 2)
			n = n / 2
			fmt.Println(n)
		case false:
			matchCount = matchCount + ((n - 1) / 2)
			n = (n - 1) / 2
			fmt.Println(n)
			n++
		}
	}
}

func even(n int) bool {
	var even bool
	n = n % 2
	if n == 0 {
		even = true
	}
	return even
}
