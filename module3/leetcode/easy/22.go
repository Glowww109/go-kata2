package easy

//1365. How Many Numbers Are Smaller Than the Current Number

func smallerNumbersThanCurrent(nums []int) []int {
	var counter int
	var result []int
	for i := 0; i < len(nums); i++ {
		counter = 0
		for j := 0; j < len(nums); j++ {
			if nums[i] > nums[j] {
				counter++
			}
		}
		result = append(result, counter)
	}
	return result
}
