package easy

//1720. Decode XORed Array

func decode(encoded []int, first int) []int {
	//у нас есть 2 массива: encoded и arr (длиной+1)
	//нам нужно перекодировать encoded в arr. При этом одинаковые значения - 0, разные - 1(xor)
	//надо использовать ^ (xor)

	//П.С. в марафоне решения задачек, ставить такую под конец - издевательство)

	arr := make([]int, len(encoded)+1)
	arr[0] = first

	for i := 0; i < len(encoded); i++ {
		arr[i+1] = encoded[i] ^ arr[i]
	}

	return arr
}
