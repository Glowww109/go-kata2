package easy

//2114. Maximum Number of Words Found in Sentences
import "strings"

func mostWordsFound(sentences []string) int {
	var maxCounter int
	for _, sentence := range sentences {
		counter := strings.Count(sentence, " ") + 1
		if maxCounter < counter {
			maxCounter = counter
		}
	}
	return maxCounter
}
