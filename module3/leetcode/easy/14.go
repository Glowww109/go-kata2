package easy

//1672. Richest Customer Wealth

func maximumWealth(accounts [][]int) int {
	//Повезло, эта уже была как-то решена на литкод)
	var sum int
	var sumMax int
	for _, wealth := range accounts {
		sum = 0
		for _, wealthNum := range wealth {
			sum += wealthNum
		}
		if sum > sumMax {
			sumMax = sum
		}
	}
	return sumMax
}
