package easy

// 1480. Running Sum of 1d Array
func runningSum(nums []int) []int {
	var sums = make([]int, len(nums))
	for i := 0; i < len(nums); i++ {
		//в цикле нужно добавить к sum[i] то, что будет как nums[i-1], пока мы не дойдём до i == 0.
		var sum int
		if i != 0 {
			for j := i; j >= 0; j-- {
				sum += nums[j]
			}
		} else {
			sum = nums[i]
		}
		sums[i] = sum
	}
	return sums
}
