package easy

//1108. Defanging an IP Address
import "regexp"

func defangIPaddr(address string) string {
	var result string
	match := regexp.MustCompile("[.]")
	result = match.ReplaceAllString(address, "[.]")

	return result
}
