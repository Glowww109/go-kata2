package easy

//1603. Design Parking System

type ParkingSystem struct {
	bigMax        int
	mediumMax     int
	smallMax      int
	counterBig    int
	counterMedium int
	counterSmall  int
}

func Constructor(big int, medium int, small int) ParkingSystem {
	park := ParkingSystem{
		bigMax:    big,
		mediumMax: medium,
		smallMax:  small,
	}
	return park
}

func (park *ParkingSystem) AddCar(carType int) bool {
	//Если место есть, получаем подтверждение, если нет - отказ.
	//если место подтверждено, машина должна припарковаться и повысить счетчик на 1.
	//если счетчик равен максимальному числа машин на соответствующей парковке, то выдаётся отказ
	//учитывать общее колиичество заявленных машин не будем

	switch carType {
	case 1:
		park.counterBig++
		if park.counterBig > park.bigMax {
			return false
		}
	case 2:
		park.counterMedium++
		if park.counterMedium > park.mediumMax {
			return false
		}
	case 3:
		park.counterSmall++
		if park.counterSmall > park.smallMax {
			return false
		}
	}
	return true
}
