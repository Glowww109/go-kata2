package easy

//1470. Shuffle the Array

func shuffle(nums []int, n int) []int {
	x := 0
	y := n
	arr := make([]int, len(nums))
	for i := 0; i < len(nums); i++ {
		if i%2 == 0 {
			arr[i] = nums[x]
			x++
		} else {
			arr[i] = nums[y]
			y++
		}
	}
	return arr
}
