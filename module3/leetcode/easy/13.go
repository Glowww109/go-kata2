package easy

//771. Jewels and Stones
import "strings"

func numJewelsInStones(jewels string, stones string) int {
	//нам нужно посчитать сколько в stones находится символов jewels.
	//Для этого нам надо разбить jewels на отдельные символы и сравнить каждый из них с символами stones
	//тут можно использовать strings.Count или через сравнение.
	var counter int
	for _, jewel := range jewels {
		counter += strings.Count(stones, string(jewel))
	}
	return counter
}
