package easy

// 2469. Convert the Temperature
func convertTemperature(celsius float64) []float64 {
	var ans = make([]float64, 2)

	kelvin := celsius + 273.15
	ans[0] = kelvin
	fahrenheit := (celsius + 1.8) + 32.00
	ans[1] = fahrenheit

	return ans
}
