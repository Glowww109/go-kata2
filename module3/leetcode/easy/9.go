package easy

//2011. Final Value of Variable After Performing Operations
import "strings"

func finalValueAfterOperations(operations []string) int {
	var x int
	for _, operation := range operations {
		if strings.Contains(operation, "+") {
			x++
		} else {
			x--
		}
	}
	return x
}
