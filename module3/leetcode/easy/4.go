package easy

// 1920. Build Array from Permutation
func buildArray(nums []int) []int {
	var ans = make([]int, len(nums))

	for i := 0; i < len(nums); i++ {
		ans[i] = nums[nums[i]]
	}
	return ans
}
