package easy

//1137. N-th Tribonacci Number

func tribonacci(n int) int {
	var firstNumb int
	var secondNumb int
	var thirdNumb int
	var result int

	if n == 1 {
		return 1
	}

	for i := 1; i < n; i++ {
		firstNumb = secondNumb
		secondNumb = thirdNumb
		thirdNumb = result
		if thirdNumb == 0 {
			thirdNumb = 1
		}

		result = firstNumb + secondNumb + thirdNumb

	}
	return result
}
