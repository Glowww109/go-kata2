package easy

//804. Unique Morse Code Words
import (
	"fmt"
)

func uniqueMorseRepresentations(words []string) int {
	var morzeMap = map[string]string{"a": ".-", "b": "-...", "c": "-.-.", "d": "-..", "e": ".", "f": "..-.", "g": "--.", "h": "....", "i": "..", "j": ".---", "k": "-.-", "l": ".-..", "m": "--", "n": "-.", "o": "---", "p": ".--.", "q": "--.-", "r": ".-.", "s": "...", "t": "-", "u": "..-", "v": "...-", "w": ".--", "x": "-..-", "y": "-.--", "z": "--.."}
	var count int

	var morzeWords = make(map[int]string)
	for i, word := range words {
		count++
		var morzeWord string
		for i := 0; i < len(word); i++ {
			morzeWord += morzeMap[string(word[i])]
		}
		for k, val := range morzeWords {
			if morzeWord == val {
				count--
				morzeWords[k] = ""
			}
		}
		morzeWords[i] = morzeWord
		fmt.Println(morzeWords)
	}
	return count
}
