package easy

//1678. Goal Parser Interpretation

import "regexp"

func interpret(command string) string {
	var result string
	str1, _ := regexp.Compile(`\(\)`)
	str2, _ := regexp.Compile(`\(al\)`)
	result = str1.ReplaceAllString(command, "o")
	result = str2.ReplaceAllString(result, "al")

	return result
}
