package easy

//2535. Difference Between Element Sum and Digit Sum of an Array
import "strconv"

func differenceOfSum(nums []int) int {
	var elems int
	var numbers int

	for i := 0; i < len(nums); i++ {
		elems += nums[i]
		numberStr := strconv.Itoa(nums[i])
		if nums[i]/10 < 0 {
			numbers += nums[i]
		} else {
			for _, str := range numberStr {
				number, _ := strconv.Atoi(string(str))
				numbers += number
			}
		}
	}
	elems -= numbers
	return elems
}
