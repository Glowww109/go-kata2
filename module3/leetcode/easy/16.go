package easy

//2413. Smallest Even Multiple

func smallestEvenMultiple(n int) int {
	//Нужно вернуть наименьшее кратное число, кратное и 2 и n. Проверить кратность можно разделив через %

	for i := 1; i <= n*10; i++ {
		switch {
		case i%2 == 0 && i%n == 0:
			return i
		default:
			continue
		}
	}
	return 0
}
