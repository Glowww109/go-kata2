package easy

// 1512. Number of Good Pairs
func numIdenticalPairs(nums []int) int {
	var counter int
	for i := 0; i < len(nums); i++ {
		//нам нужно каждое i сравнить с j и, если условие выполняется - увеличить счетчик хороших пар на 1.
		for j := i; j < len(nums); j++ {
			if nums[i] == nums[j] && i < j {
				counter++
			}
		}
	}
	return counter
}
