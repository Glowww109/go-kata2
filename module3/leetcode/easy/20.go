package easy

//1431. Kids With the Greatest Number of Candies

func kidsWithCandies(candies []int, extraCandies int) []bool {
	var result = make([]bool, len(candies))
	var maxKidWithCandies int

	for i := 0; i < len(candies); i++ {
		if maxKidWithCandies < candies[i] {
			maxKidWithCandies = candies[i]
		}
	}

	for i := 0; i < len(candies); i++ {
		switch {
		case (candies[i] + extraCandies) > maxKidWithCandies:
			result[i] = true
		default:
			result[i] = false
		}
	}
	return result
}
