package easy

//1281. Subtract the Product and Sum of Digits of an Integer

func subtractProductAndSum(n int) int {
	sum := 0
	digits := 1

	for n > 0 {
		numb := n % 10
		digits = digits * numb
		sum = sum + numb
		n = n / 10
	}
	return digits - sum
}
