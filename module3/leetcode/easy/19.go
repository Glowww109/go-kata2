package easy

//2160. Minimum Sum of Four Digit Number After Splitting Digits

import (
	"fmt"
	"sort"
)

func minimumSum(num int) int {

	var arr = make([]int, 4)

	for i := 0; i < 4; i++ {
		arr[i] = num % 10
		num = num / 10
	}
	fmt.Println(arr)

	sort.Ints(arr)
	fmt.Println(arr)
	num1 := (arr[0] * 10) + arr[3]
	num2 := (arr[1] * 10) + arr[2]

	return num1 + num2
}
