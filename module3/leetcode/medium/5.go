package medium

import "fmt"

//2130. Maximum Twin Sum of a Linked List

// крайне низкий рантайм)
func pairSum(head *ListNode) int {
	var maxSum int
	var array = []int{head.Val}

	//переводим всё в слайс
	for head.Next != nil {
		head = head.Next
		array = append(array, head.Val)
	}
	fmt.Println(array)

	if len(array) == 2 {
		maxSum = array[0] + array[1]
		return maxSum
	}
	for i := 0; i < len(array)/2; i++ {
		sum := array[i] + array[len(array)-i-1]

		if sum > maxSum {
			maxSum = sum
		}
	}

	return maxSum
}
