package medium

import "fmt"

//1557. Minimum Number of Vertices to Reach All Nodes

func findSmallestSetOfVertices(n int, edges [][]int) []int {
	//очень много потратил времени, просто чтобы понять, что требуется
	//нашёл решение через подсказку, с проверкой на вход
	connectVertices := make([]bool, n)
	for _, edge := range edges {
		connectVertices[edge[1]] = true
	}
	fmt.Println(connectVertices)
	var result []int
	for i, v := range connectVertices {
		if !v {
			result = append(result, i)
		}
	}
	return result
}
