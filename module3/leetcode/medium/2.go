package medium

//2545. Sort the Students by Their Kth Score

func sortTheStudents(score [][]int, k int) [][]int {
	//Нам дан многомерный массив, который нужно будет в будущем отсортировать в нужном порядке
	//Сортировка, думаю, будет bubble sort.
	swapped := true
	c := 1
	for swapped {
		swapped = false
		for j := 0; j < len(score)-c; j++ {
			if score[j][k] < score[j+1][k] {
				score[j], score[j+1] = score[j+1], score[j]
				swapped = true
			}
		}
		c++
	}
	return score
}
