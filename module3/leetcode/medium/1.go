package medium

import "fmt"

// Definition for a binary tree node.
type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func deepestLeavesSum(root *TreeNode) int {
	maxDegree := GetMaxDepth(root)
	fmt.Println(maxDegree)
	result := GetMaxValue(root, 1, maxDegree)
	fmt.Println(result)
	return result
}

func GetMaxDepth(root *TreeNode) int {
	//вычисление глубины бинарного дерева
	if root == nil {
		return 0
	}
	left := GetMaxDepth(root.Left)
	right := GetMaxDepth(root.Right)
	if left > right {
		return left + 1
	} else {
		return right + 1
	}
}

func GetMaxValue(root *TreeNode, depth, maxDepth int) int {
	if root == nil {
		return 0
	}
	if depth == maxDepth {
		return root.Val
	}
	return GetMaxValue(root.Left, depth+1, maxDepth) + GetMaxValue(root.Right, depth+1, maxDepth)
}
