package medium

//2181. Merge Nodes in Between Zeros

// Definition for singly-linked list.
type ListNode struct {
	Val  int
	Next *ListNode
}

func mergeNodes(head *ListNode) *ListNode {
	newNode := &ListNode{}
	currentNode := newNode
	sum := 0
	for head != nil {
		if head.Val == 0 && sum != 0 {
			newNode := &ListNode{
				Val:  sum,
				Next: nil,
			}
			currentNode.Next = newNode
			currentNode = currentNode.Next
			sum = 0
		}
		sum += head.Val
		head = head.Next

	}
	newNode = newNode.Next
	return newNode
}
