package medium

//1382. Balance a Binary Search Tree

func balanceBST(root *TreeNode) *TreeNode {
	var nodes []*TreeNode
	sliceNodes(root, &nodes)
	return newBalanceTree(nodes)
}

func newBalanceTree(nodes []*TreeNode) *TreeNode {
	if len(nodes) == 0 {
		return nil
	}
	if len(nodes) == 1 {
		nodes[0].Left, nodes[0].Right = nil, nil
		return nodes[0]
	}
	newRoot := nodes[len(nodes)/2]
	newRoot.Left = newBalanceTree(nodes[:len(nodes)/2])
	newRoot.Right = newBalanceTree(nodes[len(nodes)/2+1:])
	return newRoot
}

func sliceNodes(node *TreeNode, nodes *[]*TreeNode) {
	if node == nil {
		return
	}
	if node.Left != nil {
		sliceNodes(node.Left, nodes)
	}
	*nodes = append(*nodes, node)
	if node.Right != nil {
		sliceNodes(node.Right, nodes)
	}
}
