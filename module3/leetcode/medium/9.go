package medium

import "sort"

//1630. Arithmetic Subarrays

func checkArithmeticSubarrays(nums []int, l []int, r []int) []bool {
	result := make([]bool, len(l))
	for i := 0; i < len(l); i++ {
		result[i] = true
		if r[i]-l[i] > 0 {
			var temp []int
			temp = append(temp, nums[l[i]:r[i]+1]...)
			sort.Ints(temp)

			var counter = temp[1] - temp[0]
			for j := 1; j < len(temp); j++ {
				if temp[j]-temp[j-1] != counter {
					result[i] = false
					break
				}
			}
		} else {
			result[i] = false
		}
	}
	return result
}
