package medium

//654. Maximum Binary Tree

func constructMaximumBinaryTree(nums []int) *TreeNode {
	if len(nums) == 0 {
		return nil
	}
	max, idx := maxNum(nums)
	return &TreeNode{max, constructMaximumBinaryTree(nums[:idx]), constructMaximumBinaryTree(nums[idx+1:])}
}

func maxNum(nums []int) (max, idx int) {
	for i, val := range nums {
		if i == 0 || val > max {
			max = val
			idx = i
		}
	}
	return
}
