package medium

//1038. Binary Search Tree to Greater Sum Tree

* Definition for a binary tree node.
type TreeNode struct {
	Val int
	Left *TreeNode
	Right *TreeNode
}


func bstToGst(root *TreeNode) *TreeNode {
	if root == nil {
		return root
	}

	sumBST(root, 0)

	return root
}

func sumBST(root *TreeNode, sum int) int {
	if root.Right != nil {
		sum = sumBST(root.Right, sum)
	}

	root.Val += sum
	sum = root.Val

	if root.Left != nil {
		sum = sumBST(root.Left, sum)
	}

	return sum
}
