package main

//В конце работы поменять main на task!!!
//как в примере!

import (
	"bytes"
	"encoding/json"
	"fmt"
	"math/rand"
	"os"
	"time"
)

type DoubleLinkedList struct {
	head *Node // начальный элемент в списке
	tail *Node // последний элемент в списке
	curr *Node // текущий элемент меняется при использовании методов next, prev
	len  int   // количество элементов в списке
}

type Node struct {
	data *Commit
	prev *Node
	next *Node
}

type Commit struct {
	Message string    `json:"message"`
	UUID    string    `json:"uuid"`
	Date    time.Time `json:"date"`
}

type LinkedLister interface {
	Len() int
	Current() *Node
	Next() *Node
	Prev() *Node
	LoadData(path string) error
	Insert(n int, c Commit) error
	Delete(n int) error
	DeleteCurrent() error
	Index() (int, error)
	Pop() *Node
	Shift() *Node
	SearchUUID(uuID string) *Node
	Search(message string) *Node
	Reverse() *DoubleLinkedList
}

// LoadData загрузка данных из подготовленного json файла
func (d *DoubleLinkedList) LoadData(path string) error {
	var com []Commit
	fileJSON, _ := os.ReadFile(path)

	//делать решил через буфер, через декодер
	buf := new(bytes.Buffer)
	buf.Write(fileJSON)

	decoder := json.NewDecoder(buf)
	err := decoder.Decode(&com)

	checkError(err)

	// отсортировать список используя самописный QuickSort
	com = QuickSort(com)

	//// добавление данных в списки
	for i := 0; i < len(com); i++ {
		newNode := &Node{data: &com[i]}

		if d.head == nil {

			//secondNode := &Node{data: &com[i+1]}

			d.head = newNode
			d.tail = newNode
			d.curr = newNode

			//secondNode := &Node{data: &com[i+1]}
			//
			//newNode.next = secondNode
			//d.curr.next = secondNode

		} else {
			err := d.Insert(i, com[i])
			_ = err
			newNode := &Node{data: &com[i]}

			currentNode := d.head

			for currentNode.next != nil {
				currentNode = currentNode.next
			}

			newNode.prev = currentNode
			currentNode.next = newNode

			d.curr = newNode
			//d.curr.prev = currentNode
			//d.curr.next = newNode

			d.tail = newNode
		}
		d.len++
		fmt.Println(d.tail.data)
	}

	return nil
}

// Len получение длины списка
func (d *DoubleLinkedList) Len() int {
	return d.len
}

// Current получение текущего элемента
func (d *DoubleLinkedList) Current() *Node {
	if d.head == nil {
		return nil
	}
	return d.curr
}

// Next получение следующего элемента
func (d *DoubleLinkedList) Next() *Node {
	if d.head == nil {
		return nil
	}
	d.curr = d.curr.next
	return d.curr
}

// Prev получение предыдущего элемента
func (d *DoubleLinkedList) Prev() *Node {
	if d.head == nil {
		return nil
	}
	d.curr = d.curr.prev
	return d.curr
}

// Insert вставка элемента после n элемента
func (d *DoubleLinkedList) Insert(n int, c Commit) error {
	newNode := &Node{
		data: &c,
		prev: nil,
		next: nil,
	}
	//var counter int

	if d.head == nil {
		d.head = newNode
		d.tail = newNode
		d.curr = newNode

		d.len = 1
		return nil
	} else {
		currElem := d.head
		for i := 0; i < n; i++ {
			currElem = currElem.next
		}

		nextElem := currElem.next
		currElem.next = newNode
		newNode.prev = currElem
		newNode.next = nextElem
		if d.tail == nil {
			d.tail = newNode
		} else {
			nextElem.prev = newNode
		}

	}
	return nil
}

// Delete удаление n элемента
func (d *DoubleLinkedList) Delete(n int) error {
	panic("implement me")
}

// DeleteCurrent удаление текущего элемента
func (d *DoubleLinkedList) DeleteCurrent() error {

	d.curr.prev.next = d.curr.next
	d.curr.next.prev = d.curr.prev

	d.len--

	return nil
}

// Index получение индекса текущего элемента
func (d *DoubleLinkedList) Index() (int, error) {
	startList := d.head
	currList := d.curr
	var index int
	for i := 0; i < d.len; i++ {
		if currList == startList {
			index = i
			break
		}
		startList = startList.next
	}
	return index, nil
}

// Pop Операция Pop
func (d *DoubleLinkedList) Pop() *Node {
	panic("implement me")
}

// Shift операция shift
func (d *DoubleLinkedList) Shift() *Node {
	panic("implement me")
}

// SearchUUID поиск коммита по uuid
func (d *DoubleLinkedList) SearchUUID(uuID string) *Node {
	startList := d.head
	for i := 0; i < d.len; i++ {
		if startList.data.UUID == uuID {
			return startList
		}
		startList = startList.next
	}
	return nil
}

// Search поиск коммита по message
func (d *DoubleLinkedList) Search(message string) *Node {
	startList := d.head
	for i := 0; i < d.len; i++ {
		if startList.data.Message == message {
			return startList
		}
		startList = startList.next
	}
	return nil
}

// Reverse возвращает перевернутый список
func (d *DoubleLinkedList) Reverse() *DoubleLinkedList {
	panic("implement me")
}

func GenerateJSON() {
	// Дополнительное задание написать генератор данных
	// используя библиотеку gofakeit
}

func checkError(err error) {
	if err != nil {
		panic("что-то не работает")
	}
}

func QuickSort(com []Commit) []Commit {
	if len(com) <= 1 {
		return com
	}
	median := com[rand.Intn(len(com))]

	lowPart := make([]Commit, 0, len(com))
	middlePart := make([]Commit, 0, len(com))
	highPart := make([]Commit, 0, len(com))

	for _, item := range com {
		switch {
		case item.Date.Before(median.Date):
			lowPart = append(lowPart, item)
		case item.Date.After(median.Date):
			highPart = append(highPart, item)
		case item.Date.Equal(median.Date):
			middlePart = append(middlePart, item)
		}
	}
	lowPart = QuickSort(lowPart)
	highPart = QuickSort(highPart)

	lowPart = append(lowPart, middlePart...)
	lowPart = append(lowPart, highPart...)

	return lowPart
}

func main() {
	lists := &DoubleLinkedList{}

	_ = lists.LoadData("./module3/task/test.json")

	var com1 = Commit{
		Message: "hello",
		UUID:    "123",
		Date:    time.Time{},
	}

	//fmt.Printf("длина cписка: %d листов\n", lists.Len())
	//fmt.Println("первый элемент ", lists.head.next.data)
	//fmt.Println("последний элемент ", lists.tail.data)
	//fmt.Println("текущий элемент ", lists.curr.data)
	//
	//fmt.Println("___________________")
	//fmt.Println(lists.Current().data)
	//fmt.Println(lists.Current().prev.prev.prev.prev.prev.data)
	////
	//fmt.Println(lists.Search("Try to quantify the SMS firewall, maybe it will bypass the optical application!"))

	fmt.Println("------------------------------------------------")
	//// добавление данных в списки
	//
	//var com []Commit
	//fileJSON, _ := os.ReadFile("./module3/task/test.json")
	//
	////делать решил через буфер, через декодер
	//buf := new(bytes.Buffer)
	//buf.Write(fileJSON)
	//
	//decoder := json.NewDecoder(buf)
	//err := decoder.Decode(&com)
	//
	//checkError(err)
	//
	//// отсортировать список используя самописный QuickSort
	//com = QuickSort(com)
	//
	//for i := 0; i < len(com); i++ {
	//	newNode := &Node{data: &com[i]}
	//
	//	if lists.head == nil {
	//
	//		//secondNode := &Node{data: &com[i+1]}
	//
	//		lists.head = newNode
	//		lists.tail = newNode
	//		lists.curr = newNode
	//
	//		//secondNode := &Node{data: &com[i+1]}
	//		//
	//		//newNode.next = secondNode
	//		//d.curr.next = secondNode
	//
	//	} else {
	//		newNode := &Node{data: &com[i]}
	//
	//		currentNode := lists.head
	//
	//		for currentNode.next != nil {
	//			currentNode = currentNode.next
	//		}
	//
	//		newNode.prev = currentNode
	//		currentNode.next = newNode
	//
	//		lists.curr = newNode
	//		//d.curr.prev = currentNode
	//		//d.curr.next = newNode
	//
	//		lists.tail = newNode
	//	}
	//	lists.len++
	//
	//}
	//fmt.Println(lists.Current().data)
	//fmt.Println(lists.Prev().data)
	err := lists.Insert(10, com1)
	_ = err
}
