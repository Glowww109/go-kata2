package main

//В конце работы поменять main на task!!!
//как в примере!

import (
	"bytes"
	"encoding/json"
	"fmt"
	"math/rand"
	"os"
	"time"
)

type DoubleLinkedList struct {
	head *Node // начальный элемент в списке
	tail *Node // последний элемент в списке
	curr *Node // текущий элемент меняется при использовании методов next, prev
	len  int   // количество элементов в списке
}

type Node struct {
	data *Commit
	prev *Node
	next *Node
}

type Commit struct {
	Message string    `json:"message"`
	UUID    string    `json:"uuid"`
	Date    time.Time `json:"date"`
}

type LinkedLister interface {
	Len() int
	Current() *Node
	Next() *Node
	Prev() *Node
	LoadData(path string) error
	Insert(n int, c Commit) error
	Delete(n int) error
	DeleteCurrent() error
	Index() (int, error)
	Pop() *Node
	Shift() *Node
	SearchUUID(uuID string) *Node
	Search(message string) *Node
	Reverse() *DoubleLinkedList
}

// LoadData загрузка данных из подготовленного json файла
func (d *DoubleLinkedList) LoadData(path string) error {
	var com []Commit
	fileJSON, _ := os.ReadFile(path)

	//делать решил через буфер, через декодер
	buf := new(bytes.Buffer)
	buf.Write(fileJSON)

	decoder := json.NewDecoder(buf)
	err := decoder.Decode(&com)

	checkError(err)

	// отсортировать список используя самописный QuickSort
	com = QuickSort(com)

	//// добавление данных в списки
	for _, elem := range com {
		if d.head == nil {
			node := &Node{
				data: &elem,
				prev: nil,
				next: nil,
			}

			d.head = node
			//для того, чтобы изначально был один current лист
			d.curr = node
			d.tail = node

			d.len++

			continue
		}

		node := &Node{
			data: &elem,
			prev: d.tail,
			next: nil,
		}

		d.tail = node
		d.len++

		fmt.Println(d.curr.data)
	}
	return nil
}

// Len получение длины списка
func (d *DoubleLinkedList) Len() int {
	return d.len
}

// Current получение текущего элемента
func (d *DoubleLinkedList) Current() *Node {
	if d.head == nil {
		return nil
	}
	return d.curr
}

// Next получение следующего элемента
func (d *DoubleLinkedList) Next() *Node {
	if d.head == nil {
		return nil
	}
	d.curr = d.curr.next
	return d.curr
}

// Prev получение предыдущего элемента
func (d *DoubleLinkedList) Prev() *Node {
	if d.head == nil {
		return nil
	}
	return d.curr.prev
}

// Insert вставка элемента после n элемента
func (d *DoubleLinkedList) Insert(n int, c Commit) error {
	panic("implement me")
}

// Delete удаление n элемента
func (d *DoubleLinkedList) Delete(n int) error {
	panic("implement me")
}

// DeleteCurrent удаление текущего элемента
func (d *DoubleLinkedList) DeleteCurrent() error {
	panic("implement me")
}

// Index получение индекса текущего элемента
func (d *DoubleLinkedList) Index() (int, error) {
	panic("implement me")
}

// Pop Операция Pop
func (d *DoubleLinkedList) Pop() *Node {
	panic("implement me")
}

// Shift операция shift
func (d *DoubleLinkedList) Shift() *Node {
	panic("implement me")
}

// SearchUUID поиск коммита по uuid
func (d *DoubleLinkedList) SearchUUID(uuID string) *Node {
	panic("implement me")
}

// Search поиск коммита по message
func (d *DoubleLinkedList) Search(message string) *Node {
	panic("implement me")
}

// Reverse возвращает перевернутый список
func (d *DoubleLinkedList) Reverse() *DoubleLinkedList {
	panic("implement me")
}

func GenerateJSON() {
	// Дополнительное задание написать генератор данных
	// используя библиотеку gofakeit
}

func checkError(err error) {
	if err != nil {
		panic("что-то не работает")
	}
}

func QuickSort(com []Commit) []Commit {
	if len(com) <= 1 {
		return com
	}
	median := com[rand.Intn(len(com))]

	lowPart := make([]Commit, 0, len(com))
	middlePart := make([]Commit, 0, len(com))
	highPart := make([]Commit, 0, len(com))

	for _, item := range com {
		switch {
		case item.Date.Before(median.Date):
			lowPart = append(lowPart, item)
		case item.Date.After(median.Date):
			highPart = append(highPart, item)
		case item.Date.Equal(median.Date):
			middlePart = append(middlePart, item)
		}
	}
	lowPart = QuickSort(lowPart)
	highPart = QuickSort(highPart)

	lowPart = append(lowPart, middlePart...)
	lowPart = append(lowPart, highPart...)

	return lowPart
}

func main() {
	lists := &DoubleLinkedList{}

	_ = lists.LoadData("./module3/task/test.json")

	fmt.Println(lists.Next())
	//func (d *DoubleLinkedList) Len() int
	fmt.Printf("длина cписка: %d листов\n", lists.Len())
}
